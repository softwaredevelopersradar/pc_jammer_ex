﻿

namespace CommandLibrary
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;

    using WatsonTcp;

    using DataTypes;
    using Resources;
    using Resources.Enum;


    /// <summary>
    /// Client side command handler. Allows to form correct <see cref="Package"/> for further processing on the server side.
    /// </summary>
    public class CommandReceiver : IDisposable
    {
        /// <summary>
        /// The package counter.
        /// </summary>
        private byte packageCounter;

        /// <summary>
        /// Server <see cref="IPEndPoint"/>
        /// </summary>
        private IPEndPoint serverEndPoint;

        /// <summary>
        /// Initializes a new instance of the <see cref="CommandReceiver"/> class.
        /// </summary>
        /// <param name="data">
        /// The data.
        /// </param>
        /// <param name="senderAddress">
        /// The sender address.
        /// </param>
        /// <param name="receiverAddress">
        /// The receiver address.
        /// </param>
        public CommandReceiver(P327Data data, byte senderAddress, byte receiverAddress)
        {
            this.serverEndPoint = new IPEndPoint(IPAddress.Loopback, 8080);
            this.Server = new WatsonTcpServer(this.serverEndPoint.Address.ToString(), this.serverEndPoint.Port);
            this.Data = data;
            this.SenderAddress = senderAddress;
            this.ReceiverAddress = receiverAddress;
            this.packageCounter = 0;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CommandReceiver"/> class.
        /// </summary>
        /// <param name="data">
        /// The data.
        /// </param>
        /// <param name="senderAddress">
        /// The sender address.
        /// </param>
        /// <param name="receiverAddress">
        /// The receiver address.
        /// </param>
        /// <param name="serverEndPoint">
        /// The server ip address and port. <see cref="IPEndPoint"/>
        /// </param>
        public CommandReceiver(P327Data data, byte senderAddress, byte receiverAddress, IPEndPoint serverEndPoint)
        {
            this.serverEndPoint = serverEndPoint;
            this.Server = new WatsonTcpServer(this.serverEndPoint.Address.ToString(), this.serverEndPoint.Port);
            this.Data = data;
            this.SenderAddress = senderAddress;
            this.ReceiverAddress = receiverAddress;
            this.packageCounter = 0;
        }

        /// <summary>
        /// Gets the P327 database.
        /// </summary>
        public P327Data Data { get; }

        /// <summary>
        /// Gets or sets address of the tcp server to connect.
        /// </summary>
        public IPEndPoint ServerEndPoint
        {
            get => this.serverEndPoint;
            set => this.serverEndPoint = value;
        }

        /// <summary>
        /// Gets or sets a property used to customize the display of messages showed in <see cref="Logger"/>.
        /// </summary>
        public FormatType LoggerFormat { get; set; } = FormatType.Full;

        /// <summary>
        /// Method to invoke when sending a log message.
        /// </summary>
        public Action<Severity, string> Logger
        {
            get => this.Server.Settings.Logger;
            set => this.Server.Settings.Logger = value;
        }

        /// <summary>
        /// Gets or sets events related to tcp connection.
        /// </summary>
        public WatsonTcpServerEvents TcpEvents
        {
            get => this.Server?.Events;
            set
            {
                if (this.Server != null)
                {
                    this.Server.Events = value;
                }
            }
        }

        /// <summary>
        /// Gets <see cref="server"/>
        /// </summary>
        public WatsonTcpServer Server { get; private set; }

        /// <summary>
        /// Gets or sets address of the product forming the request command
        /// </summary>
        public byte SenderAddress { get; set; }

        /// <summary>
        /// Gets or sets address of the product for which the request is intended.
        /// </summary>
        public byte ReceiverAddress { get; set; }


        /// <summary>
        /// Initiate a tcp server.
        /// </summary>
        public void StartTcpServer()
        {
            try
            {
                this.Server.Events.MessageReceived += this.OnDataReceived;
                this.Server.Callbacks.SyncRequestReceived += this.OnDataReceived;
                this.Server.Start();
#if NETCOREAPP || NET5_0
                if ((Environment.OSVersion.Platform == PlatformID.Win32NT) && Environment.OSVersion.Version.Major < 10)
                {
                    this.Server.Keepalive.EnableTcpKeepAlives = false;
                    //doesn't work properly on win7.
                }
                else
                {
                    this.Server.Keepalive.EnableTcpKeepAlives = true;
                }

#elif NETFRAMEWORK
                this.Server.Keepalive.EnableTcpKeepAlives = true;
#elif NETSTANDARD
                this.Server.Keepalive.EnableTcpKeepAlives = false;
                this.Logger?.Invoke(Severity.Alert, "Tcp keep alives not available in netstandard.");
#endif
            }
            catch (InvalidOperationException e)
            {
                this.Logger?.Invoke(Severity.Error, e.Message);
            }
            catch
            {
                // ignored
            }
        }



        public void StopServer()
        {
            this.Server.DisconnectClients();
            this.Server.Stop();
        }

        public void Dispose()
        {
            this.Server?.Dispose();
        }

        private SyncResponse OnDataReceived(SyncRequest req)
        {
            var package = Package.Parse(req.Data);
            if (package == null)
            {
                return new SyncResponse(req,
                    this.DefaultAnswer(CommandCodes.StatusRequest, (byte) ErrorCodes.ProcessingError).DataBytes);
            }
            var responsePackage = this.OnDataReceived(req.Data);
            this.Logger?.Invoke(Severity.Info,
                $"Request received: {(CommandCodes) package.CommandId} {package.ToString(LoggerFormat)} \n");
            return new SyncResponse(req, responsePackage.ToByteArray());
        }

        private void OnDataReceived(object sender, MessageReceivedEventArgs messageReceivedEventArgs)
        {
            var responsePackage = this.OnDataReceived(messageReceivedEventArgs.Data);
            this.Server?.Send(messageReceivedEventArgs.IpPort, responsePackage.ToByteArray());
        }

        /// <summary>
        /// Action on data received.
        /// </summary>
        private Package OnDataReceived(byte[] data)
        {
            Package e = Package.Parse(data);
            if (e == null)
            {
                return this.DefaultAnswer(CommandCodes.StatusRequest, (byte) ErrorCodes.ProcessingError);
            }
            this.ReceiverAddress = e.SenderAddress;
                switch ((CommandCodes)e.CommandId)
                {
                    case CommandCodes.TestCommand:
                        {
                            return this.TestCommand();
                        }

                    case CommandCodes.TextCommand:
                        {
                            return this.DefaultAnswer(CommandCodes.TextCommandAnswer, (byte)ErrorCodes.Ok);
                        }

                    case CommandCodes.SetTimeCommand:
                        {
                            return this.SetTime(e);
                        }

                    case CommandCodes.SetOperMode:
                        {
                            return this.SetOperatingMode(e);
                        }

                    case CommandCodes.SetRadioReconnRange:
                        {
                            return this.SetRadioReconnaissanceRange(e);
                        }

                    case CommandCodes.SetRadioReconnSector:
                        {
                            return this.SetRadioReconnaissanceSector(e);
                        }

                    case CommandCodes.AssignmentOfDampingBands:
                        {
                            return this.AssignmentOfDampingBands(e);
                        }

                    case CommandCodes.SetProhibFreqForDampingBands:
                        {
                            return this.SetProhibitedFreqForDampingBands(e);
                        }

                    case CommandCodes.SetImpFreqForReconn:
                        {
                            return this.SetImportantFreqForRadioReconnaissance(e);
                        }

                    case CommandCodes.SetKnownFreqReconn:
                        {
                            return this.SetKnownFreqForRadioReconnaissance(e);
                        }

                    case CommandCodes.SaveData:
                        {
                            this.Data.WriteInFile();
                            return this.DefaultAnswer((CommandCodes)e.CommandId, (byte)ErrorCodes.Ok);
                        }

                    case CommandCodes.AssigningFixedOperFreqForRadioSup:
                        {
                            return this.AssigningFixedOperFreqForRadioSup(e);
                        }

                    case CommandCodes.AssigningFreqWithProgramConvForRadioSup:
                        {
                            return this.AssigningFreqWithProgramConvForRadioSup(e);
                        }

                    case CommandCodes.StatusRequest:
                        {
                            var dataToSend = new List<byte>
                                                 {
                                                     (byte)ErrorCodes.Ok,
                                                     this.Data.CurrentOperatingMode,
                                                     this.Data.ChanelControl
                                                 };
                            dataToSend.AddRange(this.Data.ServiceabilityOfLetters);
                            return this.DefaultRequestAnswer(CommandCodes.StatusRequest, dataToSend.ToArray());
                        }

                    case CommandCodes.CoordRequest:
                        {
                            var dataToSend = new List<byte> { (byte)ErrorCodes.Ok };
                            dataToSend.AddRange(this.Data.Coordinates);
                            return this.DefaultRequestAnswer(CommandCodes.CoordRequest, dataToSend.ToArray());
                        }

                    case CommandCodes.RequestForFixFreqRadioSources:
                        {
                            var dataToSend = new List<byte> { (byte)ErrorCodes.Ok };
                            dataToSend.AddRange(this.Data.DataEmFixedFreq.ToArray());
                            return this.DefaultRequestAnswer(CommandCodes.RequestForFixFreqRadioSources, dataToSend.ToArray());
                        }

                    case CommandCodes.RequestForProgramConvRadioSources:
                        {
                            var dataToSend = new List<byte> { (byte)ErrorCodes.Ok };
                            dataToSend.AddRange(this.Data.DataEmSoftwareOpFreq.ToArray());
                            return this.DefaultRequestAnswer(CommandCodes.RequestForProgramConvRadioSources, dataToSend.ToArray());
                        }

                    case CommandCodes.RequestForExecutiveDirectionFinding:
                        {
                            return this.GetExecutiveDirectionFindingData(e);
                        }

                    case CommandCodes.RequestForQuasiTemporalDirectionFinding:
                        {
                            return this.GetQuasiTemporalDirectionFindingData(e);
                        }

                    case CommandCodes.RequestStateOfRadioSupOfFixedFreqSources:
                        {
                            return this.GetStateOfRadioSupOfFixedFreqSources(e);
                        }

                    case CommandCodes.RequestStateOfRadioSupWithProgConvSources:
                        {
                            return this.GetStateOfRadioSupWithProgConvSources(e);
                        }

                    default:
                        {
                            return this.DefaultAnswer((CommandCodes)e.CommandId, (byte)ErrorCodes.ProcessingError);
                        }
                }
        }

        public Package GetStateOfRadioSupWithProgConvSources(Package incomingPackage)
        {
            var dataToSend = new List<byte> { (byte)ErrorCodes.Ok };
            dataToSend.AddRange(this.Data.PprchJammingData.ToArray());

            return this.DefaultRequestAnswer(CommandCodes.RequestStateOfRadioSupWithProgConvSources, dataToSend.ToArray());
        }

        public Package GetStateOfRadioSupOfFixedFreqSources(Package incomingPackage)
        {
            var dataToSend = new List<byte> { (byte)ErrorCodes.Ok };
            foreach (var data in this.Data.IRiJammingData)
            {
                dataToSend.AddRange(data.ToArray());
            }

            return this.DefaultRequestAnswer(CommandCodes.RequestStateOfRadioSupOfFixedFreqSources, dataToSend.ToArray());
        }

        public Package GetQuasiTemporalDirectionFindingData(Package incomingPackage)
        {
            this.Data.QuasiSimultaneousDirectionFindingFreq = incomingPackage.DataBytes;
            this.Data.WriteInFile();
            var dataToSend = new List<byte> { (byte)ErrorCodes.Ok };
            dataToSend.AddRange(this.Data.QuasiSimultaneousDirectionFindingFreq);
            dataToSend.AddRange(this.Data.AverageBearingMaster.ToArray());
            dataToSend.AddRange(this.Data.QuasiSimultaneousDirectionFindingFreq);
            dataToSend.AddRange(this.Data.AverageBearingSlave.ToArray());
            return this.DefaultRequestAnswer(CommandCodes.RequestForQuasiTemporalDirectionFinding, dataToSend.ToArray());
        }

        public Package GetExecutiveDirectionFindingData(Package incomingPackage)
        {
            this.Data.ExecutiveDirectionFindingFreq = incomingPackage.DataBytes;
            this.Data.WriteInFile();
            var dataToSend = new List<byte> { (byte)ErrorCodes.Ok };
            dataToSend.AddRange(this.Data.ExecutiveDirectionFindingFreq);
            dataToSend.AddRange(this.Data.AverageBearingExDir.ToArray());
            return this.DefaultRequestAnswer(CommandCodes.RequestForExecutiveDirectionFinding, dataToSend.ToArray());
        }

        public Package TestCommand()
        {
            var dataList = new List<byte> { (byte)ErrorCodes.Ok, this.Data.CurrentOperatingMode, this.Data.ChanelControl };
            dataList.AddRange(this.Data.ServiceabilityOfLetters);
            var package = new Package(
                this.SenderAddress,
                this.ReceiverAddress,
                (byte)CommandCodes.TestCommand,
                this.packageCounter++,
                dataList.ToArray());
            return package;
        }

        public Package SetTime(Package incomingPackage)
        {
            this.Data.CurrentTime = incomingPackage.DataBytes;
            this.Data.WriteInFile();
            var dataList = new List<byte> { (byte)ErrorCodes.Ok };
            dataList.AddRange(this.Data.CurrentTime);
            var package = new Package(
                this.SenderAddress,
                this.ReceiverAddress,
                (byte)CommandCodes.SetTimeCommand,
                this.packageCounter++,
                dataList.ToArray());
            return package;
        }

        public Package SetOperatingMode(Package incomingPackage)
        {
            this.Data.CurrentOperatingMode = incomingPackage.DataBytes[0];
            this.Data.WriteInFile();
            var package = new Package(
                this.SenderAddress,
                this.ReceiverAddress,
                (byte)CommandCodes.SetOperMode,
                this.packageCounter++,
                new[] { (byte)ErrorCodes.Ok });
            return package;
        }

        public Package SetRadioReconnaissanceRange(Package incomingPackage)
        {
            try
            {
                var frequencies = Frequency.ArrayToObjects(incomingPackage.DataBytes);
                var freqMin = new List<byte>();
                var freqMax = new List<byte>();
                foreach (var frequency in frequencies)
                {
                    freqMin.AddRange(frequency.MinFreq);
                    freqMax.AddRange(frequency.MaxFreq);
                }

                this.Data.RadioReconnFreqMin = freqMin.ToArray();
                this.Data.RadioReconnFreqMax = freqMax.ToArray();
                this.Data.WriteInFile();
                return this.DefaultAnswer(CommandCodes.SetRadioReconnRange, (byte)ErrorCodes.Ok);
            }
            catch
            {
                return this.DefaultAnswer(CommandCodes.SetRadioReconnRange, (byte)ErrorCodes.WritingToDatabaseError);
            }
        }

        public Package SetRadioReconnaissanceSector(Package incomingPackage)
        {
            try
            {
                var angles = Angle.ArrayToObjects(incomingPackage.DataBytes);
                var angleMin = new List<byte>();
                var angleMax = new List<byte>();
                foreach (var angle in angles)
                {
                    angleMin.AddRange(angle.MinFreq);
                    angleMax.AddRange(angle.MaxFreq);
                }

                this.Data.RadioReconnSectorAngleMin = angleMin.ToArray();
                this.Data.RadioReconnSectorAngleMax = angleMax.ToArray();
                this.Data.WriteInFile();
                return this.DefaultAnswer(CommandCodes.SetRadioReconnSector, (byte)ErrorCodes.Ok);
            }
            catch
            {
                return this.DefaultAnswer(CommandCodes.SetRadioReconnSector, (byte)ErrorCodes.WritingToDatabaseError);
            }
        }

        public Package AssignmentOfDampingBands(Package incomingPackage)
        {
            try
            {
                var frequencies = Frequency.ArrayToObjects(incomingPackage.DataBytes);
                var freqMin = new List<byte>();
                var freqMax = new List<byte>();
                foreach (var frequency in frequencies)
                {
                    freqMin.AddRange(frequency.MinFreq);
                    freqMax.AddRange(frequency.MaxFreq);
                }

                this.Data.DampingFreqMin = freqMin.ToArray();
                this.Data.DampingFreqMax = freqMax.ToArray();
                this.Data.WriteInFile();
                return this.DefaultAnswer(CommandCodes.AssignmentOfDampingBands, (byte)ErrorCodes.Ok);
            }
            catch
            {
                return this.DefaultAnswer(CommandCodes.AssignmentOfDampingBands, (byte)ErrorCodes.WritingToDatabaseError);
            }
        }

        public Package SetProhibitedFreqForDampingBands(Package incomingPackage)
        {
            try
            {
                var frequencies = Frequency.ArrayToObjects(incomingPackage.DataBytes);
                var freqMin = new List<byte>();
                var freqMax = new List<byte>();
                foreach (var frequency in frequencies)
                {
                    freqMin.AddRange(frequency.MinFreq);
                    freqMax.AddRange(frequency.MaxFreq);
                }

                this.Data.ProhibDampingFreqMin = freqMin.ToArray();
                this.Data.ProhibDampingFreqMax = freqMax.ToArray();
                this.Data.WriteInFile();
                return this.DefaultAnswer(CommandCodes.SetProhibFreqForDampingBands, (byte)ErrorCodes.Ok);
            }
            catch
            {
                return this.DefaultAnswer(CommandCodes.SetProhibFreqForDampingBands, (byte)ErrorCodes.WritingToDatabaseError);
            }
        }

        public Package SetImportantFreqForRadioReconnaissance(Package incomingPackage)
        {
            try
            {
                var frequencies = Frequency.ArrayToObjects(incomingPackage.DataBytes);
                var freqMin = new List<byte>();
                var freqMax = new List<byte>();
                foreach (var frequency in frequencies)
                {
                    freqMin.AddRange(frequency.MinFreq);
                    freqMax.AddRange(frequency.MaxFreq);
                }

                this.Data.ImpFreqForReconnMin = freqMin.ToArray();
                this.Data.ImpFreqForReconnMax = freqMax.ToArray();
                this.Data.WriteInFile();
                return this.DefaultAnswer(CommandCodes.SetImpFreqForReconn, (byte)ErrorCodes.Ok);
            }
            catch
            {
                return this.DefaultAnswer(CommandCodes.SetImpFreqForReconn, (byte)ErrorCodes.WritingToDatabaseError);
            }
        }

        public Package SetKnownFreqForRadioReconnaissance(Package incomingPackage)
        {
            try
            {
                var frequencies = Frequency.ArrayToObjects(incomingPackage.DataBytes);
                var freqMin = new List<byte>();
                var freqMax = new List<byte>();
                foreach (var frequency in frequencies)
                {
                    freqMin.AddRange(frequency.MinFreq);
                    freqMax.AddRange(frequency.MaxFreq);
                }

                this.Data.KnownFreqForReconnMin = freqMin.ToArray();
                this.Data.KnownFreqForReconnMax = freqMax.ToArray();
                this.Data.WriteInFile();
                return this.DefaultAnswer(CommandCodes.SetKnownFreqReconn, (byte)ErrorCodes.Ok);
            }
            catch
            {
                return this.DefaultAnswer(CommandCodes.SetKnownFreqReconn, (byte)ErrorCodes.WritingToDatabaseError);
            }
        }

        public Package AssigningFixedOperFreqForRadioSup(Package incomingPackage)
        {
            try
            {
                Array.Copy(
                    incomingPackage.DataBytes,
                    0,
                    this.Data.RadiationDuration,
                    0,
                    4);
                var dataWithoutRadDurationBytes = incomingPackage.DataBytes.Skip(4).ToArray();
                var supFreq = SuppressionFrequencies.ArrayToObjects(dataWithoutRadDurationBytes);
                var frequencies = new List<byte>();
                var modulationCode = new List<byte>();
                var deviationCode = new List<byte>();
                var manipulationCode = new List<byte>();
                var durationCode = new List<byte>();
                var priorityCode = new List<byte>();
                var thresholdValue = new List<byte>();

                foreach (var obj in supFreq)
                {
                    frequencies.AddRange(obj.Freq);
                    modulationCode.Add(obj.ModulationCode);
                    deviationCode.Add(obj.DeviationCode);
                    manipulationCode.Add(obj.ManipulationCode);
                    durationCode.Add(obj.DurationCode);
                    priorityCode.Add(obj.Priority);
                    thresholdValue.Add(obj.ThresholdValue);
                }

                this.Data.SreFreqValues = frequencies.ToArray();
                this.Data.ModulationCode = modulationCode.ToArray();
                this.Data.ManipulationCode = manipulationCode.ToArray();
                this.Data.DurationCode = durationCode.ToArray();
                this.Data.DeviationCode = deviationCode.ToArray();
                this.Data.PriorityCode = priorityCode.ToArray();
                this.Data.ThresholdValue = thresholdValue.ToArray();
                this.Data.WriteInFile();
            }
            catch
            {
                return this.DefaultAnswer(CommandCodes.AssigningFixedOperFreqForRadioSup, (byte)ErrorCodes.WritingToDatabaseError);
            }

            return this.DefaultAnswer(CommandCodes.AssigningFixedOperFreqForRadioSup, (byte)ErrorCodes.Ok);
        }

        public Package AssigningFreqWithProgramConvForRadioSup(Package incomingPackage)
        {
            try
            {
                Array.Copy(
                    incomingPackage.DataBytes,
                    0,
                    this.Data.RadiationDuration,
                    0,
                    3);
                Array.Copy(
                    incomingPackage.DataBytes,
                    3,
                    this.Data.SreFreqValues,
                    0,
                    4);
                this.Data.IriFftResolutionCode = incomingPackage.DataBytes[7];
                this.Data.IriModulationCode = incomingPackage.DataBytes[8];
                this.Data.IriDeviationCode = incomingPackage.DataBytes[9];
                this.Data.IriManipulationCode = incomingPackage.DataBytes[10];
                this.Data.IriThresholdValue = incomingPackage.DataBytes[11];
                this.Data.WriteInFile();
            }
            catch
            {
                return this.DefaultAnswer(CommandCodes.AssigningFreqWithProgramConvForRadioSup, (byte)ErrorCodes.WritingToDatabaseError);
            }

            return this.DefaultAnswer(CommandCodes.AssigningFreqWithProgramConvForRadioSup, (byte)ErrorCodes.Ok);
        }

        public Package DefaultRequestAnswer(CommandCodes commandCode, byte[] data)
        {
            var package = new Package(
                this.SenderAddress,
                this.ReceiverAddress,
                (byte)commandCode,
                this.packageCounter++,
                data);
            return package;
        }

        public Package DefaultAnswer(CommandCodes commandCode, byte errorCode)
        {
            var package = new Package(
                this.SenderAddress,
                this.ReceiverAddress,
                (byte)commandCode,
                this.packageCounter++,
                new[] { errorCode });
            return package;
        }


    }
}