﻿

namespace CommandLibrary
{
    using System;
    using System.Net;
    using System.Collections.Generic;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;

    using Resources;
    using Resources.Enum;

    using DataTypes;
    using WatsonTcp;

    /// <summary>
    /// Client side command handler. Allows to form correct <see cref="Package"/> for further processing on the server side.
    /// </summary>
    public class CommandSender : IDisposable
    {
        /// <summary>
        /// The package counter.
        /// </summary>
        private byte packageCounter;

        /// <summary>
        /// Address of the tcp server to connect.
        /// </summary>
        private IPEndPoint remoteEndPoint = new IPEndPoint(IPAddress.Loopback, 8090);

        /// <summary>
        /// Contains re-sending cancel tokens for each command.
        /// </summary>
        private Dictionary<CommandCodes, CancellationTokenSource> stopTokenSources;

        // private Package lastCommandSent;

        /// <summary>
        /// Initializes a new instance of the <see cref="CommandSender"/> class.
        /// </summary>
        /// <param name="senderAddress">
        /// The address of the product forming the request command
        /// </param>
        /// <param name="receiverAddress">
        /// The address of the product for which the request is intended.
        /// </param>
        public CommandSender(byte senderAddress, byte receiverAddress)
        {
            this.Client.Keepalive.TcpKeepAliveRetryCount = 10;
            this.Client = new WatsonTcpClient(this.remoteEndPoint.Address.ToString(), this.remoteEndPoint.Port);
            this.SenderAddress = senderAddress;
            this.ReceiverAddress = receiverAddress;
            this.stopTokenSources = new Dictionary<CommandCodes, CancellationTokenSource>();
            this.packageCounter = 0;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CommandSender"/> class.
        /// </summary>
        /// <param name="senderAddress">
        /// The address of the product forming the request command
        /// </param>
        /// <param name="receiverAddress">
        /// The address of the product for which the request is intended.
        /// </param>
        /// <param name="remoteEndPoint">
        /// The address:port of TCP server to connect.
        /// </param>
        public CommandSender(byte senderAddress, byte receiverAddress, IPEndPoint remoteEndPoint)
        {
            this.remoteEndPoint = remoteEndPoint;
            this.Client = new WatsonTcpClient(remoteEndPoint.Address.ToString(), this.remoteEndPoint.Port);
            this.SenderAddress = senderAddress;
            this.ReceiverAddress = receiverAddress;
            this.stopTokenSources = new Dictionary<CommandCodes, CancellationTokenSource>();
            this.packageCounter = 0;

        }

        /// <summary>
        /// The event is triggered if remote address of tcp server changed.
        /// </summary>
        protected event EventHandler RemoteEndPointChanged;

        /// <summary>
        /// Gets or sets address of the product forming the request command.
        /// </summary>
        public byte SenderAddress { get; set; }

        /// <summary>
        /// Gets or sets address of the product for which the request is intended.
        /// </summary>
        public byte ReceiverAddress { get; set; }

        /// <summary>
        /// Gets or sets address of the tcp server to connect.
        /// </summary>
        public IPEndPoint RemoteEndPoint
        {
            get => this.remoteEndPoint;
            set
            {
                this.remoteEndPoint = value;
                RemoteEndPointChanged?.Invoke(this, new EventArgs());
            }
        }

        /// <summary>
        /// Gets of sets time to wait for a response to a request.
        /// </summary>
        public int Timeout { get; set; } = 5000;

        /// <summary>
        /// Gets or sets events related to tcp connection.
        /// </summary>
        public WatsonTcpClientEvents TcpEvents
        {
            get => this.Client?.Events;
            set 
            {
                if (this.Client != null)
                {
                    this.Client.Events = value;
                }
            }
        }

        /// <summary>
        /// Gets or sets a property used to customize the display of messages showed in <see cref="Logger"/>.
        /// </summary>
        public FormatType LoggerFormat { get; set; } = FormatType.Full;

        /// <summary>
        /// Method to invoke when sending a log message.
        /// </summary>
        public Action<Severity, string> Logger
        {
            get
            {
                return this.Client.Settings.Logger;
            }
            set
            {
                this.Client.Settings.Logger = value;
            }
        }

        /// <summary>
        /// Gets <see cref="client"/>
        /// </summary>
        public WatsonTcpClient Client { get; private set; }

        /// <summary>
        /// Initiate connection to TCP server.
        /// </summary>
        public void Connect()
        {
            try
            {
                this.Client.Connect();
                this.RemoteEndPointChanged += (sender, args) =>
                {
                    this.Client.Disconnect();
                    this.Client.Dispose();
                    this.Client = new WatsonTcpClient(this.remoteEndPoint.Address.ToString(), this.remoteEndPoint.Port);
                    this.Client.Connect();
                };
#if NET5_0
                if (Environment.OSVersion.Platform == PlatformID.Win32NT && Environment.OSVersion.Version.Major < 10)
                {
                    this.Client.Keepalive.EnableTcpKeepAlives = false;
                    //doesn't work properly on win7.
                }
                else
                {
                    this.Client.Keepalive.EnableTcpKeepAlives = true;
                }
#elif NETFRAMEWORK
                this.Client.Keepalive.EnableTcpKeepAlives = true;
#elif NETSTANDARD
                this.Client.Keepalive.EnableTcpKeepAlives = false;
                this.Logger?.Invoke(Severity.Alert, "Tcp keep alives not available in netstandard.");
#endif

            }
            catch (InvalidOperationException e)
            {
                this.Logger?.Invoke(Severity.Error, e.Message);
            }
            catch
            {
                // ignored
            }
           
        }

        public void Disconnect()
        {
            try
            {
                this.Client.Disconnect();

            }
            catch (InvalidOperationException e)
            {
                this.Logger.Invoke(Severity.Alert, e.Message);
            }
            catch (NullReferenceException e)
            {
            }
        }



        /// <summary>
        /// Completes the execution of all operations related to the <see cref="CommandSender"/>.
        /// </summary>
        public void Dispose()
        {
            foreach (var item in this.stopTokenSources.Values)
            {
                item.Cancel();
            }
            this.stopTokenSources.Clear();
            this.RemoteEndPointChanged = null;
            this.Client.Dispose();
        }

        private void SendRequest(Package package)
        {
            Task.Run(() =>
            {
                //todo what to do with responsePackage? use only for log?
                try
                {
                    SyncResponse response = this.Client?.SendAndWait(this.Timeout, package.ToByteArray());
                    var responsePackage = Package.Parse(response?.Data);
                    this.Logger?.Invoke(Severity.Info,
                        $"Response received: {(CommandCodes) responsePackage.CommandId} {responsePackage.ToString(LoggerFormat)}\n");
                }
                catch (TimeoutException)
                {
                    this.Logger?.Invoke(Severity.Alert,
                        $"{(CommandCodes) package.CommandId}: no response during the specified time.\n");
                }
                catch (NullReferenceException)
                {
                    this.Logger?.Invoke(Severity.Alert,
                        $"{(CommandCodes)package.CommandId}: Network error.\n");
                }
            });
        }

        /// <summary>
        /// Upon receipt of this message, the P-327 product automatically sends
        /// an acknowledgment response code containing an error code.
        /// </summary>
        /// <param name="delayTime">
        ///     Delay time before resending in seconds. If the value is 0, the message is sent once.
        /// </param>
        public void TestCommand(int delayTime = 0)
        {

            if (delayTime != 0)
            {
                this.CancelingSendingOfCommands(CommandCodes.TestCommand);
                this.stopTokenSources.Add(CommandCodes.TestCommand, new CancellationTokenSource());
                _ = this.ResendMessagePeriodically(
                    () => this.TestCommand(),
                    TimeSpan.FromSeconds(delayTime),
                    this.stopTokenSources[CommandCodes.TestCommand].Token);
            }


            var package = new Package(
                    this.SenderAddress,
                    this.ReceiverAddress,
                    (byte) CommandCodes.TestCommand,
                    this.packageCounter++,
                    null);
            this.SendRequest(package);
        }

        /// <summary>
        /// Sends a message to client.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        public void SendMessage(string message)
        {
            var package = new Package(
                this.SenderAddress,
                this.ReceiverAddress,
                (byte)CommandCodes.TextCommand,
                this.packageCounter++,
                Encoding.Unicode.GetBytes(message));
            this.SendRequest(package);
        }

        /// <summary>
        /// Upon receipt of this message, the P-327 product automatically synchronizes
        /// its time with the time indicated in the command.
        /// </summary>
        /// <param name="delayTime">
        /// Delay time before resending in seconds. If the value is 0, the message is sent once.
        /// </param>
        public void SetTime(int delayTime = 0)
        {
            if (delayTime != 0)
            {
                this.CancelingSendingOfCommands(CommandCodes.SetTimeCommand);
                this.stopTokenSources.Add(CommandCodes.SetTimeCommand, new CancellationTokenSource());
                _ = this.ResendMessagePeriodically(
                    () => this.SetTime(),
                    TimeSpan.FromSeconds(delayTime),
                    this.stopTokenSources[CommandCodes.SetTimeCommand].Token);
            }

            var package = new Package(
                this.SenderAddress,
                this.ReceiverAddress,
                (byte)CommandCodes.SetTimeCommand,
                this.packageCounter++,
                new[] { (byte)DateTime.Now.Hour, (byte)DateTime.Now.Minute, (byte)DateTime.Now.Second });
            this.SendRequest(package);
        }

        /// <summary>
        /// Upon receipt of this message, the R-327 will switch to the operating mode indicated in the message.
        /// </summary>
        /// <param name="operatingMode">
        /// Operating mode code.
        /// </param>
        public void SetOperatingMode(byte operatingMode)
        {
            var package = new Package(
                this.SenderAddress,
                this.ReceiverAddress,
                (byte)CommandCodes.SetOperMode,
                this.packageCounter++,
                new[] { operatingMode });
            this.SendRequest(package);
        }

        /// <summary>
        /// Set radio reconnaissance range.
        /// </summary>
        /// <param name="frequency">
        /// List of frequency bands
        /// </param>
        public void SetRadioReconnaissanceRange(List<Frequency> frequency)
        {
            var data = new List<byte>();
            foreach (var freq in frequency)
            {
                data.AddRange(freq.ToArray());
            }

            var package = new Package(
                this.SenderAddress,
                this.ReceiverAddress,
                (byte)CommandCodes.SetRadioReconnRange,
                this.packageCounter++,
                data.ToArray());
            this.SendRequest(package);
        }

        /// <summary>
        /// The set radio reconnaissance sector.
        /// </summary>
        /// <param name="angle">
        /// List of Angle Ranges.
        /// </param>
        public void SetRadioReconnaissanceSector(List<Angle> angle)
        {
            var data = new List<byte>();
            foreach (var freq in angle)
            {
                data.AddRange(freq.ToArray());
            }

            var package = new Package(
                this.SenderAddress,
                this.ReceiverAddress,
                (byte)CommandCodes.SetRadioReconnSector,
                this.packageCounter++,
                data.ToArray());
            this.SendRequest(package);
        }

        /// <summary>
        /// assignment of radio suppression ranges.
        /// </summary>
        /// <param name="frequency">
        /// List of frequency bands.
        /// </param>
        public void AssignmentOfDampingBands(List<Frequency> frequency)
        {
            var data = new List<byte>();
            foreach (var freq in frequency)
            {
                data.AddRange(freq.ToArray());
            }

            var package = new Package(
                this.SenderAddress,
                this.ReceiverAddress,
                (byte)CommandCodes.AssignmentOfDampingBands,
                this.packageCounter++,
                data.ToArray());
            this.SendRequest(package);
        }

        /// <summary>
        /// Assignment of prohibited frequencies for radio suppression.
        /// </summary>
        /// <param name="frequency">
        /// List of frequency bands.
        /// </param>
        public void SetProhibitedFreqForDampingBands(List<Frequency> frequency)
        {
            var data = new List<byte>();
            foreach (var freq in frequency)
            {
                data.AddRange(freq.ToArray());
            }

            var package = new Package(
                this.SenderAddress,
                this.ReceiverAddress,
                (byte)CommandCodes.SetProhibFreqForDampingBands,
                this.packageCounter++,
                data.ToArray());
            this.SendRequest(package);
        }

        /// <summary>
        /// Set important frequencies for radio reconnaissance.
        /// </summary>
        /// <param name="frequency">
        /// List of frequency bands.
        /// </param>
        public void SetImportantFreqForRadioReconnaissance(List<Frequency> frequency)
        {
            var data = new List<byte>();
            foreach (var freq in frequency)
            {
                data.AddRange(freq.ToArray());
            }

            var package = new Package(
                this.SenderAddress,
                this.ReceiverAddress,
                (byte)CommandCodes.SetImpFreqForReconn,
                this.packageCounter++,
                data.ToArray());
            this.SendRequest(package);
        }

        /// <summary>
        /// Set known frequencies for radio reconnaissance.
        /// </summary>
        /// <param name="frequency">
        /// List of frequency bands.
        /// </param>
        public void SetKnownFreqForRadioReconnaissance(List<Frequency> frequency)
        {
            var data = new List<byte>();
            foreach (var freq in frequency)
            {
                data.AddRange(freq.ToArray());
            }

            var package = new Package(
                this.SenderAddress,
                this.ReceiverAddress,
                (byte)CommandCodes.SetKnownFreqReconn,
                this.packageCounter++,
                data.ToArray());
            this.SendRequest(package);
        }

        /// <summary>
        /// Assignment of a list of fixed operating frequencies to radio suppression.
        /// </summary>
        /// <param name="radiationDuration">
        /// Radiation duration values.
        /// </param>
        /// <param name="frequencies">
        /// List of frequency bands.
        /// </param>
        public void AssigningFixedOperFreqForRadioSup(
            List<byte> radiationDuration,
            List<SuppressionFrequencies> frequencies)
        {
            var data = new List<byte>();
            data.AddRange(radiationDuration);
            foreach (var frequency in frequencies)
            {
                data.AddRange(frequency.ToArray());
            }

            var package = new Package(
                this.SenderAddress,
                this.ReceiverAddress,
                (byte)CommandCodes.AssigningFixedOperFreqForRadioSup,
                this.packageCounter++,
                data.ToArray());
            this.SendRequest(package);
        }

        /// <summary>
        /// Assignment of a list of frequencies with software tuning of operating frequencies for radio suppression.
        /// </summary>
        /// <param name="radiationDuration">
        /// The radiation duration values.
        /// </param>
        /// <param name="freqValues">
        /// The frequencies values.
        /// </param>
        /// <param name="fftResolutionCode">
        /// The FFT resolution code.
        /// </param>
        /// <param name="modulationCode">
        /// The modulation code.
        /// </param>
        /// <param name="deviationCode">
        /// The deviation code.
        /// </param>
        /// <param name="manipulationCode">
        /// The manipulation code.
        /// </param>
        /// <param name="thresholdValue">
        /// The threshold value.
        /// </param>
        public void AssigningFreqWithProgramConvForRadioSup(
            byte[] radiationDuration,
            byte[] freqValues,
            byte fftResolutionCode,
            byte modulationCode,
            byte deviationCode,
            byte manipulationCode,
            byte thresholdValue)
        {
            var data = new List<byte>();
            data.AddRange(radiationDuration);
            data.AddRange(freqValues);
            data.AddRange(
                new[] { fftResolutionCode, modulationCode, deviationCode, manipulationCode, thresholdValue });
            var package = new Package(
                this.SenderAddress,
                this.ReceiverAddress,
                (byte)CommandCodes.AssigningFreqWithProgramConvForRadioSup,
                this.packageCounter++,
                data.ToArray());
            this.SendRequest(package);
        }

        /// <summary>
        /// Request for data depending on the <see cref="CommandCodes"/>
        /// </summary>
        /// <param name="code">
        /// The code.
        /// </param>
        /// <param name="delayTime">
        /// Delay time before resending in seconds. If the value is 0, the message is sent once.
        /// </param>
        public void DataRequest(CommandCodes code, int delayTime = 0)
        {
            if (delayTime != 0)
            {
                this.CancelingSendingOfCommands(code);
                this.stopTokenSources.Add(code, new CancellationTokenSource());
                _ = this.ResendMessagePeriodically(
                    () => this.DataRequest(code),
                    TimeSpan.FromSeconds(delayTime),
                    this.stopTokenSources[code].Token);
            }

            var package = new Package(
                this.SenderAddress,
                this.ReceiverAddress,
                (byte)code,
                this.packageCounter++,
                null);
            this.SendRequest(package);
        }

        /// <summary>
        /// Request for the result of suppression of the list of frequencies assigned to radio suppression.
        /// </summary>
        /// <param name="code">
        /// Depending on the code, the result of suppression of fixed frequencies or with software frequency tuning is output
        /// </param>
        /// <param name="letter">
        /// The letter code.
        /// </param>
        /// <param name="delayTime">
        /// Delay time before resending in seconds. If the value is 0, the message is sent once.
        /// </param>
        public void RequestStateOfRadioSupSources(CommandCodes code, byte letter, int delayTime = 0)
        {
            if (delayTime != 0)
            {
                this.CancelingSendingOfCommands(code);
                this.stopTokenSources.Add(code, new CancellationTokenSource());
                _ = this.ResendMessagePeriodically(
                    () => this.RequestStateOfRadioSupSources(code, letter),
                    TimeSpan.FromSeconds(delayTime),
                    this.stopTokenSources[code].Token);
            }

            var package = new Package(
                this.SenderAddress,
                this.ReceiverAddress,
                (byte)code,
                this.packageCounter++,
                new[] { letter });
            this.SendRequest(package);
        }

        /// <summary>
        /// The direction finding request.
        /// </summary>
        /// <param name="code">
        /// Bearing type code.
        /// </param>
        /// <param name="frequencies">
        /// Frequencies values.
        /// </param>
        public void DirectionFindingRequest(CommandCodes code, byte[] frequencies)
        {
            var package = new Package(
                this.SenderAddress,
                this.ReceiverAddress,
                (byte)code,
                this.packageCounter++,
                frequencies);
            this.SendRequest(package);
        }

        /// <summary>
        /// Sending a query to the <see cref="P327Data"/> to save all fields to an xml file
        /// </summary>
        public void SaveData()
        {
            var package = new Package(
                this.SenderAddress,
                this.ReceiverAddress,
                (byte)CommandCodes.SaveData,
                this.packageCounter++,
                new byte[] { });
            this.SendRequest(package);
        }

        /// <summary>
        /// Canceling automatic sending of commands
        /// </summary>
        /// <param name="commandCode">
        /// Command code.
        /// </param>
        /// <returns>
        /// Returns true if the command is stopped.
        /// </returns>
        public bool CancelingSendingOfCommands(CommandCodes commandCode)
        {
            if (this.stopTokenSources.ContainsKey(commandCode))
            {
                this.stopTokenSources[commandCode].Cancel();
                return this.stopTokenSources.Remove(commandCode);
            }

            return false;
        }

        /// <summary>
        /// Method for sending a message at a given interval.
        /// </summary>
        /// <param name="messageFunc">
        /// Method for re-sending.
        /// </param>
        /// <param name="interval">
        /// Re-sending interval.
        /// </param>
        /// <param name="cancellationToken">
        /// Stop re-sending token.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task ResendMessagePeriodically(
            Action messageFunc,
            TimeSpan interval,
            CancellationToken cancellationToken)
        {
            while (!cancellationToken.IsCancellationRequested)
            {
                messageFunc();
                await Task.Delay(interval, cancellationToken).ConfigureAwait(false);
            } 
        }



        /*
        private void ResendPackage()
        {
            this.DataReadyToSend?.Invoke(this, this.lastCommandSent);
        }
        */
    }
}