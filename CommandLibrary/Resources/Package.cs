﻿
namespace CommandLibrary
{
    using System;
    using System.IO;
    using Resources.Enum;

    /// <summary>
    /// Communication format between modules P327 and P330.
    /// </summary>
    public class Package
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Package"/> class.
        /// </summary>
        /// <param name="senderAddress">
        /// The sender address.
        /// </param>
        /// <param name="receiverAddress">
        /// The receiver address.
        /// </param>
        /// <param name="commandId">
        /// Command id. 
        /// </param>
        /// <param name="packageCounter">
        /// The package counter.
        /// </param>
        /// <param name="dataBytes">
        /// Data bytes.
        /// </param>
        public Package(byte senderAddress, byte receiverAddress, byte commandId, byte packageCounter, byte[] dataBytes)
        {
            this.SenderAddress = senderAddress;
            this.ReceiverAddress = receiverAddress;
            this.CommandId = commandId;
            this.PackageCounter = packageCounter;
            this.DataBytes = dataBytes ?? new byte[] { };
            this.DataLength = (byte)this.DataBytes.Length;
        }

        /// <summary>
        /// The sender IP address.
        /// </summary>
        public byte SenderAddress { get; set; }

        /// <summary>
        /// The receiver IP address.
        /// </summary>
        public byte ReceiverAddress { get; set; }

        /// <summary>
        /// Command ID.
        /// </summary>
        public byte CommandId { get; set; }

        /// <summary>
        /// Package counter.
        /// </summary>
        public byte PackageCounter { get; set; }

        /// <summary>
        /// <see cref="DataBytes"/> length.
        /// </summary>
        public byte DataLength { get; set; }

        /// <summary>
        /// Data array.
        /// </summary>
        public byte[] DataBytes { get; set; }

        /// <summary>
        /// Parse byte array to <see cref="Package"/> class.
        /// </summary>
        /// <param name="incomingBytes">
        /// The incoming bytes.
        /// </param>
        /// <returns>
        /// Returns new instance of the <see cref="Package"/> class with filled fields.
        /// Returns null if header and ending contains invalid values.
        /// </returns>
        public static Package Parse(byte[] incomingBytes)
        {
            try
            {
                if (incomingBytes == null)
                {
                    return null;
                }
                if (incomingBytes.Length < 5)
                {
                    return null;
                }

                var data = new byte[incomingBytes[4]];
                Array.Copy(incomingBytes, 5, data, 0, incomingBytes[4]);
                return new Package(incomingBytes[0], incomingBytes[1], incomingBytes[2], incomingBytes[3], data);
            }
            catch
            {
                return null;
            }
           
        }

        /// <summary>
        /// Convert packet to byte array. Indicates the beginning and end of the packet with the corresponding bytes.
        /// </summary>
        /// <returns>
        /// Data array containing all the fields of the <see cref="Package"/>
        /// </returns>
        public byte[] ToByteArray()
        {
            var packet = new MemoryStream();
            packet.Write(
                new byte[]
                    {
                        this.SenderAddress, this.ReceiverAddress, this.CommandId, this.PackageCounter, this.DataLength
                    },
                0,
                5);
            packet.Write(this.DataBytes, 0, this.DataLength);
            return packet.ToArray();
        }

        /// <summary>
        /// Prints all information about a package to a string
        /// </summary>
        /// <returns>
        /// Output <see cref="string"/>
        /// </returns>
        public override string ToString() => this.ToString(FormatType.Full);

        public string ToString(FormatType fmt)
        {
            switch (fmt)
            {
                case FormatType.Min:
                    return $"Package Counter:{this.PackageCounter:D}, Data:{string.Join(",", this.DataBytes)}";
                case FormatType.Compact:
                    return
                        $"Sender address:{this.SenderAddress:D}, Receiver Address:{this.ReceiverAddress:D}, Package Counter:{this.PackageCounter:D}, Data Length:{this.DataLength:D}";
                case FormatType.Hex:
                    {
                        var output = $"{this.SenderAddress:x2} {this.ReceiverAddress:x2} {this.PackageCounter:x2} {this.DataLength:x2}";
                        foreach (var data in this.DataBytes)
                        {
                            output += $" {data:x2}";
                        }

                        return output;
                    }
                case FormatType.Full:
                    {
                        return string.Format(
                            "Sender address:{0:D}, Receiver Address:{1:D}, CommandID:{2:D}, Package Counter:{3:D}, Data Length:{4:D}, Data:{5}",
                               this.SenderAddress,
                               this.ReceiverAddress,
                               this.CommandId,
                               this.PackageCounter,
                               this.DataLength,
                               string.Join(",", this.DataBytes));
                    }

                default:
                    return this.ToString();
            }
        }
    }
}

