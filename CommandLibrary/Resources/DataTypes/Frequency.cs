﻿namespace CommandLibrary.DataTypes
{
    using System.Collections.Generic;

    /// <summary>
    /// A class that stores frequencies data
    /// </summary>
    public class Frequency
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Frequency"/> class.
        /// </summary>
        public Frequency()
        {
            this.MinFreq = new List<byte>();
            this.MaxFreq = new List<byte>();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Frequency"/> class.
        /// </summary>
        /// <param name="minFreq">
        /// List of minimum frequencies
        /// </param>
        /// <param name="maxFreq">
        /// List of maximum frequencies
        /// </param>
        public Frequency(List<byte> minFreq, List<byte> maxFreq)
        {
            this.MinFreq = minFreq;
            this.MaxFreq = maxFreq;
        }

        /// <summary>
        /// Gets the list of minimum frequencies.
        /// </summary>
        public List<byte> MinFreq { get; }

        /// <summary>
        /// Gets the list of maximum frequencies.
        /// </summary>
        public List<byte> MaxFreq { get; }

        /// <summary>
        /// Parses an array in data into a list of <see cref="Frequency"/> objects.
        /// </summary>
        /// <param name="arrayBytes">
        /// The array bytes.
        /// </param>
        /// <returns>
        /// Parsed list of <see cref="Frequency"/> objects.
        /// </returns>
        public static List<Frequency> ArrayToObjects(byte[] arrayBytes)
        {
            var angleList = new List<Frequency>();
            for (var i = 0; i < arrayBytes.Length; i += 8)
            {
                angleList.Add(
                    new Frequency(
                        new List<byte> { arrayBytes[i], arrayBytes[i + 1], arrayBytes[i + 2], arrayBytes[i + 3] },
                        new List<byte> { arrayBytes[i + 4], arrayBytes[i + 5], arrayBytes[i + 6], arrayBytes[i + 7] }));
            }

            return angleList;
        }

        /// <summary>
        /// Writes all <see cref="MinFreq"/> and <see cref="MaxFreq"/> values ​​to a byte array.
        /// </summary>
        /// <returns>
        /// The <see cref="byte[]"/>.
        /// </returns>
        public byte[] ToArray()
        {
            var data = new List<byte>();
            data.AddRange(this.MinFreq);
            data.AddRange(this.MaxFreq);
            return data.ToArray();
        }
    }
}