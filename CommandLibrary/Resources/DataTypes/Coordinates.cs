﻿namespace CommandLibrary.DataTypes
{
    /// <summary>
    /// Class containing fields for storing coordinates
    /// </summary>
    public class Coordinates
    {
        public Coordinates()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Coordinates"/> class.
        /// </summary>
        /// <param name="longitudeSign">
        /// The longitude sign.
        /// </param>
        /// <param name="longitudeDegrees">
        /// The longitude degrees.
        /// </param>
        /// <param name="longitudeMinutes">
        /// The longitude minutes.
        /// </param>
        /// <param name="longitudeSeconds">
        /// The longitude seconds.
        /// </param>
        /// <param name="latitudeSign">
        /// The latitude sign.
        /// </param>
        /// <param name="latitudeDegrees">
        /// The latitude degrees.
        /// </param>
        /// <param name="latitudeMinutes">
        /// The latitude minutes.
        /// </param>
        /// <param name="latitudeSeconds">
        /// The latitude seconds.
        /// </param>
        public Coordinates(
            byte longitudeSign,
            byte longitudeDegrees,
            byte longitudeMinutes,
            byte longitudeSeconds,
            byte latitudeSign,
            byte latitudeDegrees,
            byte latitudeMinutes,
            byte latitudeSeconds)
        {
            this.LongitudeSign = longitudeSign;
            this.LongitudeDegrees = longitudeDegrees;
            this.LongitudeMinutes = longitudeMinutes;
            this.LongitudeSeconds = longitudeSeconds;
            this.LatitudeSign = latitudeSign;
            this.LatitudeDegrees = latitudeDegrees;
            this.LatitudeMinutes = latitudeMinutes;
            this.LatitudeSeconds = latitudeSeconds;
        }

        public byte LongitudeSign { get; set; } = 0;

        public byte LongitudeDegrees { get; set; } = 27;

        public byte LongitudeMinutes { get; set; } = 33;

        public byte LongitudeSeconds { get; set; } = 0;

        public byte LatitudeSign { get; set; } = 0;

        public byte LatitudeDegrees { get; set; } = 53;

        public byte LatitudeMinutes { get; set; } = 54;

        public byte LatitudeSeconds { get; set; } = 39;

        /// <summary>
        /// Stores a coordinates in the following form
        /// {Longitude sign, Longitude degrees, Longitude minutes,Longitude seconds,
        /// Latitude sign, Latitude Degrees,Latitude minutesLatitude seconds}.
        /// </summary>
        /// <returns>
        /// The <see cref="byte[]"/>.
        /// </returns>
        public byte[] ToArray()
        {
            return new[]
                       {
                           this.LongitudeSign, this.LongitudeDegrees, this.LongitudeMinutes, this.LongitudeSeconds,
                           this.LatitudeSign, this.LatitudeDegrees, this.LatitudeMinutes, this.LatitudeSeconds
                       };
        }

    }
}