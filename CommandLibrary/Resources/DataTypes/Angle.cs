﻿namespace CommandLibrary.DataTypes
{
    using System.Collections.Generic;

    /// <summary>
    /// A class that stores angle data
    /// </summary>
    public class Angle
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Angle"/> class.
        /// </summary>
        public Angle()
        {
            this.MinFreq = new List<byte>();
            this.MaxFreq = new List<byte>();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Angle"/> class.
        /// </summary>
        /// <param name="minAngles">
        /// List of minimum angles.
        /// </param>
        /// <param name="maxAngles">
        /// List of maximum angles.
        /// </param>
        public Angle(List<byte> minAngles, List<byte> maxAngles)
        {
            this.MinFreq = minAngles;
            this.MaxFreq = maxAngles;
        }

        public List<byte> MinFreq { get; }
        public List<byte> MaxFreq { get; }

        /// <summary>
        /// Parses an array in data into a list of <see cref="Frequency"/> objects.
        /// </summary>
        /// <param name="arrayBytes">
        /// The array bytes.
        /// </param>
        /// <returns>
        /// Parsed list of <see cref="Frequency"/> objects.
        /// </returns>
        public static List<Angle> ArrayToObjects(byte[] arrayBytes)
        {
            var angleList = new List<Angle>();
            for (var i = 0; i < arrayBytes.Length; i += 4)
            {
                angleList.Add(
                    new Angle(
                        new List<byte> { arrayBytes[i], arrayBytes[i + 1] },
                        new List<byte> { arrayBytes[i + 2], arrayBytes[i + 3] }));
            }

            return angleList;
        }

        /// <summary>
        /// Writes all <see cref="MinFreq"/> and <see cref="MaxFreq"/> values ​​to a byte array.
        /// </summary>
        /// <returns>
        /// The <see cref="byte[]"/>.
        /// </returns>
        public byte[] ToArray()
        {
            var data = new List<byte>();
            data.AddRange(this.MinFreq);
            data.AddRange(this.MaxFreq);
            return data.ToArray();
        }
    }
}