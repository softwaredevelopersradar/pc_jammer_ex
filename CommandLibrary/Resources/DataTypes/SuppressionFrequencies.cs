﻿namespace CommandLibrary.DataTypes
{
    using System.Collections.Generic;

    /// <summary>
    /// The class that stores the data needed to process the SetSreOnFfRadioJamm command
    /// </summary>
    public class SuppressionFrequencies
    {
        public SuppressionFrequencies(
            List<byte> freq,
            byte modulationCode,
            byte deviationCode,
            byte manipulationCode,
            byte durationCode,
            byte priority,
            byte thresholdValue)
        {
            this.Freq = freq;
            this.ModulationCode = modulationCode;
            this.DeviationCode = deviationCode;
            this.ManipulationCode = manipulationCode;
            this.DurationCode = durationCode;
            this.Priority = priority;
            this.ThresholdValue = thresholdValue;
        }

        public List<byte> Freq { get; }

        public byte ModulationCode { get; set; }

        public byte DeviationCode { get; set; }

        public byte ManipulationCode { get; set; }

        public byte DurationCode { get; set; }

        public byte Priority { get; set; }

        public byte ThresholdValue { get; set; }

        /// <summary>
        /// Parses an array in data into a list of <see cref="SuppressionFrequencies"/> objects.
        /// </summary>
        /// <param name="arrayBytes">
        /// The array bytes.
        /// </param>
        /// <returns>
        /// Parsed list of <see cref="SuppressionFrequencies"/> objects.
        /// </returns>
        public static List<SuppressionFrequencies> ArrayToObjects(byte[] arrayBytes)
        {
            var angleList = new List<SuppressionFrequencies>();
            for (var i = 0; i < arrayBytes.Length; i += 10)
            {
                angleList.Add(
                    new SuppressionFrequencies(
                        new List<byte> { arrayBytes[i], arrayBytes[i + 1], arrayBytes[i + 2], arrayBytes[i + 3] },
                        arrayBytes[i + 4],
                        arrayBytes[i + 5],
                        arrayBytes[i + 6],
                        arrayBytes[i + 7],
                        arrayBytes[i + 8],
                        arrayBytes[i + 9]));
            }

            return angleList;
        }

        /// <summary>
        /// Writes all <see cref="SuppressionFrequencies"/> fields ​​to a byte array.
        /// </summary>
        /// <returns>
        /// The <see cref="byte[]"/>.
        /// </returns>
        public byte[] ToArray()
        {
            var data = new List<byte>();
            data.AddRange(this.Freq);
            data.AddRange(
                new[]
                    {
                        this.ModulationCode, this.DeviationCode, this.ManipulationCode, this.DurationCode,
                        this.Priority, this.ThresholdValue
                    });
            return data.ToArray();
        }
    }
}