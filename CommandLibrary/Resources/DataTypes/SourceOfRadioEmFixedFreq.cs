﻿namespace CommandLibrary.DataTypes
{
    using System.Collections.Generic;


    /// <summary>
    /// Source of radio emissions data class.
    /// </summary>
    public class SourceOfRadioEmFixedFreq
    {
        public SourceOfRadioEmFixedFreq()
        {
            this.Frequency = new byte[] { 1, 2, 3, 4 };
            this.Hour = 17;
            this.Minute = 2;
            this.Second = 23;
            this.LeadingBearing = new byte[] { 20, 40 };
            this.BearingSlave = new byte[] { 30, 60 };
            this.Coordinates = new Coordinates();
            this.SignalLevelLeading = 70;
            this.SignalLevelSlave = 90;
            this.Bandwidth = 10;
            this.ModulationType = 3;
        }

        public SourceOfRadioEmFixedFreq(byte[] frequency, byte hour, byte minute, byte second, byte[] leadingBearing, byte[] bearingSlave, Coordinates coordinates, byte signalLevelLeading, byte signalLevelSlave, byte bandwidth, byte modulationType)
        {
            this.Frequency = frequency;
            this.Hour = hour;
            this.Minute = minute;
            this.Second = second;
            this.LeadingBearing = leadingBearing;
            this.BearingSlave = bearingSlave;
            this.Coordinates = coordinates;
            this.SignalLevelLeading = signalLevelLeading;
            this.SignalLevelSlave = signalLevelSlave;
            this.Bandwidth = bandwidth;
            this.ModulationType = modulationType;
        }

        public byte[] Frequency { get; set; }

        public byte Hour { get; set; }

        public byte Minute { get; set; }

        public byte Second { get; set; }

        public byte[] LeadingBearing { get; set; }

        public byte[] BearingSlave { get; set; }

        public Coordinates Coordinates { get; set; }

        public byte SignalLevelLeading { get; set; }
        public byte SignalLevelSlave { get; set; }

        public byte Bandwidth { get; set; }

        public byte ModulationType { get; set; }

        /// <summary>
        /// Saves all fields of class <see cref="SourceOfRadioEmFixedFreq"/> to a byte array.
        /// </summary>
        /// <returns>
        /// The <see cref="byte[]"/>.
        /// </returns>
        public byte[] ToArray()
        {
            var list = new List<byte>();
            list.AddRange(this.Frequency);
            list.Add(this.Hour);
            list.Add(this.Minute);
            list.Add(this.Second);
            list.AddRange(this.LeadingBearing);
            list.AddRange(this.BearingSlave);
            list.AddRange(this.Coordinates.ToArray());
            list.AddRange(
                new byte[] { this.SignalLevelLeading, this.SignalLevelSlave, this.Bandwidth, this.ModulationType });
            return list.ToArray();
        }
    }
}
