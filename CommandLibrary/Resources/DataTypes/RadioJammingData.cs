﻿namespace CommandLibrary.DataTypes
{
    public class RadioJammingData
    {
        public byte ControlResult { get; set; }
        public byte SuppressionResult { get; set; }

        public byte[] ToArray()
        {
            return new byte[] { this.ControlResult, this.SuppressionResult };
        }

        public override string ToString()
        {
            return this.ControlResult + "," + this.SuppressionResult;
        }
    }
}