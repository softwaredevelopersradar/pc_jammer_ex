﻿namespace CommandLibrary.DataTypes
{
    using System.Collections.Generic;

    /// <summary>
    /// Source of radio emission with software adjustment of the operating frequency;
    /// </summary>
    public class SourceOfRadioEmSoftwareOpFreq
    {
        public SourceOfRadioEmSoftwareOpFreq()
        {
        }

        public SourceOfRadioEmSoftwareOpFreq(
            Frequency frequency,
            byte[] gridStep,
            byte[] pulseDuration,
            byte minutes,
            byte seconds,
            byte[] leadingBearing,
            byte[] bearingSlave,
            Coordinates coordinate,
            byte signalLevelLeading,
            byte signalLevelSlave,
            byte bandwidth,
            byte modulationType)
        {
            this.Frequency = frequency;
            this.GridStep = gridStep;
            this.PulseDuration = pulseDuration;
            this.Minutes = minutes;
            this.Seconds = seconds;
            this.LeadingBearing = leadingBearing;
            this.BearingSlave = bearingSlave;
            this.Coordinate = coordinate;
            this.SignalLevelLeading = signalLevelLeading;
            this.SignalLevelSlave = signalLevelSlave;
            this.Bandwidth = bandwidth;
            this.ModulationType = modulationType;
        }

        public Frequency Frequency { get; set; } = new Frequency();
        public byte[] GridStep { get; set; } = { 10, 20 };

        public byte[] PulseDuration { get; set; } = { 10, 20, 30, 40 };

        public byte Minutes { get; set; } = 10;

        public byte Seconds { get; set; } = 43;

        public byte[] LeadingBearing { get; set; } = { 12, 30 };

        public byte[] BearingSlave { get; set; } = { 10, 34 };

        public Coordinates Coordinate { get; set; } = new Coordinates();

        public byte SignalLevelLeading { get; set; } = 68;

        public byte SignalLevelSlave { get; set; } = 71;

        public byte Bandwidth { get; set; } = 10;

        public byte ModulationType { get; set; } = 2;

        /// <summary>
        /// Saves all fields of class <see cref="SourceOfRadioEmFixedFreq"/> to a byte array.
        /// </summary>
        /// <returns>
        /// The <see cref="byte[]"/>.
        /// </returns>
        public byte[] ToArray()
        {
            var list = new List<byte>();
            list.AddRange(this.Frequency.ToArray());
            list.AddRange(this.GridStep);
            list.AddRange(this.PulseDuration);
            list.Add(this.Minutes);
            list.Add(this.Seconds);
            list.AddRange(this.LeadingBearing);
            list.AddRange(this.BearingSlave);
            list.AddRange(this.Coordinate.ToArray());
            list.AddRange(
                new byte[] { this.SignalLevelLeading, this.SignalLevelSlave, this.Bandwidth, this.ModulationType });
            return list.ToArray();
        }
    }
}