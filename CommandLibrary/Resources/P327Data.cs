﻿
namespace CommandLibrary.Resources
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Xml.Serialization;
    using DataTypes;


    /// <summary>
    /// Data storage class for P327 module.
    /// </summary>
    [Serializable]
    public class P327Data
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="P327Data"/> class.
        /// </summary>
        public P327Data()
        {
            this.XmlFilePath = "P327Data.xml";
            this.DataEmFixedFreq = new SourceOfRadioEmFixedFreq();
            this.DataEmSoftwareOpFreq = new SourceOfRadioEmSoftwareOpFreq();
            this.IRiJammingData = new List<RadioJammingData>() { new RadioJammingData() };
            this.PprchJammingData = new RadioJammingData();
        }

        /// <summary>
        /// Gets or sets the xml file path.
        /// </summary>
        public string XmlFilePath { get; set; }

        /// <summary>
        /// Gets or sets the current operating mode.
        /// </summary>
        public byte CurrentOperatingMode { get; set; } = 0;

        /// <summary>
        /// Gets or sets the serviceability of letters.
        /// </summary>
        [XmlArrayItem("Letter")]
        public byte[] ServiceabilityOfLetters { get; set; } = { 1, 2, 3 };

        public byte ChanelControl { get; set; } = 1;

        [XmlArrayItem("Frequency")]
        public byte[] RadioReconnFreqMin { get; set; } = { 10, 20, 30, 40 };

        [XmlArrayItem("Frequency")]
        public byte[] RadioReconnFreqMax { get; set; } = { 20, 40, 60, 80 };

        [XmlArrayItem("Frequency")]
        public byte[] ImpFreqForReconnMin { get; set; } = { 10, 20, 30, 40 };

        [XmlArrayItem("Frequency")]
        public byte[] ImpFreqForReconnMax { get; set; } = { 20, 40, 60, 80 };

        [XmlArrayItem("Frequency")]
        public byte[] KnownFreqForReconnMin { get; set; } = { 10, 20, 30, 40 };

        [XmlArrayItem("Frequency")]
        public byte[] KnownFreqForReconnMax { get; set; } = { 20, 40, 60, 80 };

        [XmlArrayItem("Angle")]
        public byte[] RadioReconnSectorAngleMin { get; set; } = { 20, 40 };

        [XmlArrayItem("Angle")]
        public byte[] RadioReconnSectorAngleMax { get; set; } = { 60, 80 };

        [XmlArrayItem("Frequency")]
        public byte[] DampingFreqMin { get; set; } = { 10, 20, 30, 40 };

        [XmlArrayItem("Frequency")]
        public byte[] DampingFreqMax { get; set; } = { 20, 40, 60, 80 };

        [XmlArrayItem("Frequency")]
        public byte[] ProhibDampingFreqMin { get; set; } = { 10, 20, 30, 40 };

        [XmlArrayItem("Frequency")]
        public byte[] ProhibDampingFreqMax { get; set; } = { 20, 40, 60, 80 };

        [XmlArrayItem("Duration")]
        public byte[] RadiationDuration { get; set; } = { 20, 50, 60, 200 };

        [XmlArrayItem("Frequency")]
        public byte[] SreFreqValues { get; set; } = { 11, 22, 33, 44 };

        [XmlArrayItem("Modulation codes")]
        public byte[] ModulationCode { get; set; } = { 1 };

        [XmlArrayItem("Deviation codes")]
        public byte[] DeviationCode { get; set; } = { 3 };

        [XmlArrayItem("Manipulation codes")]
        public byte[] ManipulationCode { get; set; } = { 2 };

        [XmlArrayItem("Duration codes")]
        public byte[] DurationCode { get; set; } = { 30 };

        [XmlArrayItem("Priority codes")]
        public byte[] PriorityCode { get; set; } = { 1 };

        [XmlArrayItem("ThresholdValue")]
        public byte[] ThresholdValue { get; set; } = { 5 };

        [XmlArrayItem("Duration")]
        public byte[] IriRadiationDuration { get; set; } = { 12, 50, 200 };

        [XmlArrayItem("Frequency")]
        public byte[] IriSreFreqValues { get; set; } = { 20, 40, 60, 80 };

        public byte IriFftResolutionCode { get; set; } = 4;

        public byte IriModulationCode { get; set; } = 2;

        public byte IriDeviationCode { get; set; } = 3;

        public byte IriManipulationCode { get; set; } = 2;

        public byte IriThresholdValue { get; set; } = 3;

        /// <summary>
        /// Gets or sets the coordinates. Stores a coordinates in the following form
        /// {Longitude sign, Longitude degrees, Longitude minutes,Longitude seconds,
        /// Latitude sign, Latitude Degrees,Latitude minutesLatitude seconds}.
        /// </summary>
        [XmlArrayItem("Coordinate")]
        public byte[] Coordinates { get; set; } = { 0, 27, 33, 0, 0, 53, 54, 39 };

        /// <summary>
        /// Gets or sets the current time. Stores a time value in the following form {hours, minutes, seconds}
        /// </summary>
        [XmlArrayItem("Time")]
        public byte[] CurrentTime { get; set; } = { 10, 48, 00 };

        public SourceOfRadioEmFixedFreq DataEmFixedFreq { get; set; }

        public SourceOfRadioEmSoftwareOpFreq DataEmSoftwareOpFreq { get; set; }

        [XmlArrayItem("Frequency")]
        public byte[] ExecutiveDirectionFindingFreq { get; set; } = { 1, 10, 20, 30 };

        [XmlArrayItem("Bearing")]
        public byte[] AverageBearingExDir { get; set; } = { 1, 20 };

        [XmlArrayItem("Frequency")]
        public byte[] QuasiSimultaneousDirectionFindingFreq { get; set; } = { 1, 10, 20, 30 };

        [XmlArrayItem("Bearing")]
        public byte[] AverageBearingMaster { get; set; } = { 20, 50 };

        [XmlArrayItem("Bearing")]
        public byte[] AverageBearingSlave { get; set; } = { 23, 40 };

        public List<RadioJammingData> IRiJammingData { get; set; }
        public RadioJammingData PprchJammingData { get; set; }



        /// <summary>
        /// Creates object  from xml file.
        /// </summary>
        /// <param name="xmlFileName">
        /// Xml file path including file name.
        /// </param>
        /// <returns>
        /// Returns a new instance of the <see cref="P327Data"/> class.
        /// </returns>
        public static P327Data CreateObjectFromXmlFile(string xmlFileName)
        {
            try
            {
                XmlSerializer formatter = new XmlSerializer(typeof(P327Data));
                using (FileStream fs = new FileStream(xmlFileName, FileMode.Open))
                {
                    var data = formatter.Deserialize(fs) as P327Data;
                    return data;
                }
            }
            catch (FileNotFoundException)
            {
                return null;
            }
        }

        /// <summary>
        /// Writes all fields of an object of class <see cref="P327Data"/> to an xml file
        /// </summary>
        /// <param name="xmlFilePath">
        /// Xml file path including file name.
        /// </param>
        public void WriteInFile(string xmlFilePath = "")
        {
            try
            {
                if (!string.IsNullOrEmpty(xmlFilePath))
                {
                    this.XmlFilePath = xmlFilePath;
                }

                var formatter = new XmlSerializer(typeof(P327Data));
                using (var fs = new FileStream(this.XmlFilePath, FileMode.OpenOrCreate))
                {
                    formatter.Serialize(fs, this);
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
            }
        }
    }
}