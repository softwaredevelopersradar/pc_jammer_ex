﻿namespace CommandLibrary.Resources.Enum
{
    public enum FormatType
    {
        Min = 0,
        Compact = 1,
        Full = 2,
        Hex = 3,
    }
}