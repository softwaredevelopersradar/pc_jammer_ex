﻿namespace CommandLibrary.Resources.Enum
{
    public enum ErrorCodes : byte
    {
        Ok = 0x00,
        ProcessingError = 0x01,
        WritingToDatabaseError = 0x02,
    }
}