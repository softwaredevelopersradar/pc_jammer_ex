﻿namespace CommandLibrary.Resources.Enum
{
    public enum CommandCodes : byte
    {
        TestCommand = 0x01,
        TextCommand = 0x02,
        SetTimeCommand = 0x03,
        SetOperMode = 0x04,
        SetRadioReconnRange = 0x05,
        SetRadioReconnSector = 0x06,
        AssignmentOfDampingBands = 0x07,
        SetProhibFreqForDampingBands = 0x08,
        SetImpFreqForReconn = 0x09,
        SetKnownFreqReconn = 0x0A,

        /// <summary>
        /// Assigning a list of fixed operating frequencies for radio suppression.
        /// </summary>
        AssigningFixedOperFreqForRadioSup = 0x0B,

        /// <summary>
        /// Assigning a list of frequencies with programmable frequency converting to radio suppression.
        /// </summary>
        AssigningFreqWithProgramConvForRadioSup = 0x0C,
        StatusRequest = 0x0D,
        CoordRequest = 0x0E,

        /// <summary>
        /// Request for fixed frequency radio sources
        /// </summary>
        RequestForFixFreqRadioSources = 0x0F,

        /// <summary>
        /// Request radio emission sources with program tuning of operating frequency.
        /// </summary>
        RequestForProgramConvRadioSources = 0x10,
        RequestForExecutiveDirectionFinding = 0x11,
        RequestForQuasiTemporalDirectionFinding = 0x12,

        /// <summary>
        /// Request the state of radio suppression of fixed frequency radio sources
        /// </summary>
        RequestStateOfRadioSupOfFixedFreqSources = 0x13,

        /// <summary>
        /// Request the radio suppression state of the radio sources with the program tuning of the operating frequency.
        /// </summary>
        RequestStateOfRadioSupWithProgConvSources = 0x14,
        TextCommandAnswer = 0x0B,
        SaveData = 0xDD

    }
}