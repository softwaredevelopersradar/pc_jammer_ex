﻿
namespace GUI
{
    using System;
    using System.Collections.Generic;
    using System.Net;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using System.Windows.Media;

    using CommandLibrary;
    using CommandLibrary.Resources.Enum;
    using CommandLibrary.DataTypes;

    using WatsonTcp;


    /// <summary>
    /// Interaction logic for TcpClientWindow.xaml
    /// </summary>
    public partial class TcpClientWindow : Window
    {

        /// <summary>
        /// Command sender.
        /// </summary>
        private CommandSender commandSender;

        /// <summary>
        /// List of frequencies required for correct packet formation.
        /// </summary>
        private List<Frequency> frequencies;

        /// <summary>
        /// List of angles required for correct packet formation.
        /// </summary>
        private List<Angle> angles;

        /// <summary>
        /// The list of frequencies for suppression required for correct packet formation.
        /// </summary>
        private List<SuppressionFrequencies> supFreqs;

        /// <summary>
        /// List of delegates signed up for the event <see cref="StopSendButtonClick"/>
        /// </summary>
        private List<RoutedEventHandler> delegateEventHandlers = new List<RoutedEventHandler>();

        /// <summary>
        /// Initializes a new instance of the <see cref="TcpClientWindow"/> class.
        /// </summary>
        public TcpClientWindow()
        {
            this.InitializeComponent();
            this.IpTextBlock.Text = "127.0.0.1";
            this.PortTextBlock.Text = "808";
            this.frequencies = new List<Frequency>();
            this.angles = new List<Angle>();
            this.supFreqs = new List<SuppressionFrequencies>();
        }

        /// <summary>
        /// The <see cref="StopSend"/> click event.
        /// </summary>
        private event RoutedEventHandler StopSendButtonClick
        {
            add
            {
                this.StopSend.Click += value;
                this.delegateEventHandlers.Add(value);
            }

            remove
            {
                this.StopSend.Click -= value;
                this.delegateEventHandlers.Remove(value);
            }
        }

        public int NumberOfErrors { get; set; } = 0;

        private void AddEventHandlers()
        {
            this.CommandBox.SelectionChanged += this.ShowArgumentGrid;
            this.CommandBox.DropDownClosed += this.ShowArgumentGrid;
            this.commandSender.TcpEvents.ServerConnected += this.OnServerConnected;
            this.commandSender.TcpEvents.ServerDisconnected += this.OnServerDisconnected;
            this.commandSender.TcpEvents.MessageReceived += this.OnMessageReceived;
        }

        private void OnServerConnected(object o, ConnectionEventArgs args)
        {
            this.Dispatcher.Invoke(() =>
            {
                this.ConnectionStatusText.Text = "Online";
                this.LogBox.AddTextToLog($"Connected to {args.IpPort}");
                this.ConnectButton.Visibility = Visibility.Collapsed;
                this.StopClient.Visibility = Visibility.Visible;
                this.commandSender.SetTime();
            });
        }

        private void OnMessageReceived(object obj, MessageReceivedEventArgs package)
        {
            this.Dispatcher.Invoke(() =>
            {
                this.LogBox.AddTextToLog("Received from:" + package.IpPort + this.ByteOutput(Package.Parse(package.Data), this.LogBox.ShowAllByte), Brushes.Linen);
            });
        }

        private void OnServerDisconnected(object o, DisconnectionEventArgs args)
        {
            this.Dispatcher.Invoke(() =>
            {
                this.ConnectionStatusText.Text = "Offline";
                this.LogBox.AddTextToLog($"Disconnected from {args.IpPort}, reason {args.Reason}");
                this.StopClient.Visibility = Visibility.Collapsed;
                this.ConnectButton.Visibility = Visibility.Visible;
            });
        }

        /// <summary>
        /// Connects to the server according to the data entered in <see cref="IpTextBlock"/> and <see cref="PortTextBlock"/>
        /// </summary>
        private void ConnectButton_OnClick(object sender, RoutedEventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(this.IpTextBlock.Text) || string.IsNullOrEmpty(this.PortTextBlock.Text))
                {
                    throw new FormatException();
                }


                this.commandSender = new CommandSender(byte.Parse(this.ClientId.Text), byte.Parse(this.ServerId.Text),
                    new IPEndPoint(IPAddress.Parse(this.IpTextBlock.Text), int.Parse(this.PortTextBlock.Text)));
                this.AddEventHandlers();
                this.commandSender.Logger = this.Logger;
                this.commandSender.Connect();
            }
            catch (FormatException)
            {
                this.LogBox.AddTextToLog("Wrong ip or host value.\n", Brushes.Red);
            }
        }

        private void Logger(Severity s, string message)
        {
            this.commandSender.LoggerFormat = this.LogBox.ShowAllByte ? FormatType.Hex : FormatType.Full;
            this.Dispatcher.Invoke(() =>
            {
                switch (s)
                {
                    case Severity.Debug:
                        this.LogBox.AddTextToLog(message, Brushes.Gray);
                        break;
                    case Severity.Info:
                        this.LogBox.AddTextToLog(message, Brushes.Aquamarine);
                        break;
                    case Severity.Warn:
                        this.LogBox.AddTextToLog(message, Brushes.Yellow);
                        break;
                    case Severity.Error:
                        this.LogBox.AddTextToLog(message, Brushes.OrangeRed);
                        break;
                    case Severity.Alert:
                        this.LogBox.AddTextToLog(message, Brushes.OrangeRed);
                        this.NumberOfErrors++;
                        this.NumberOfErrorsTextBlock.Text = this.NumberOfErrors.ToString();
                        break;
                    case Severity.Critical:
                        this.LogBox.AddTextToLog(message, Brushes.Red);
                        break;
                    case Severity.Emergency:
                        this.LogBox.AddTextToLog(message, Brushes.Red);
                        break;
                    default:
                        this.LogBox.AddTextToLog(message, Brushes.Gray);
                        break;
                }
            });

        }

        private string ByteOutput(Package package, bool value)
        {
            return value
                       ? " : " + Enum.GetName(typeof(CommandCodes), package.CommandId) + " " + package.ToString(FormatType.Hex)
                         + "\n\n"
                       : " : " + Enum.GetName(typeof(CommandCodes), package.CommandId) + " " + package.ToString(FormatType.Full)
                         + "\n\n";
        }



        private void TcpClientWindow_OnClosed(object sender, EventArgs e)
        {
            this.commandSender?.Dispose();
        }

        /// <summary>
        /// Forms a <see cref="Package"/> for transmission in accordance with the command selected by the user.
        /// </summary>
        private void SendButton_OnClick(object sender, RoutedEventArgs e)
        {
            if (this.CommandBox.SelectionBoxItem.ToString() == string.Empty)
            {
                return;
            }

            try
            {
                var code = (CommandCodes)this.CommandBox.SelectionBoxItem;
                switch (code)
                {
                    case CommandCodes.TextCommand:
                    {
                        this.commandSender.SendMessage(this.TextValue.Text);
                        break;
                    }

                    case CommandCodes.TestCommand:
                    {
                        this.commandSender.TestCommand(int.Parse(this.DelayValue.Text));
                        break;
                    }

                    case CommandCodes.SetTimeCommand:
                    {
                        this.commandSender.SetTime(int.Parse(this.DelayValue.Text));
                        break;
                    }

                    case CommandCodes.SetOperMode:
                    {
                        this.commandSender.SetOperatingMode(byte.Parse(this.OperValue.Text));
                        break;
                    }

                    case CommandCodes.SetRadioReconnRange:
                    {
                        this.commandSender.SetRadioReconnaissanceRange(this.frequencies);
                        break;
                    }

                    case CommandCodes.SetRadioReconnSector:
                    {
                        this.commandSender.SetRadioReconnaissanceSector(this.angles);
                        break;
                    }

                    case CommandCodes.AssignmentOfDampingBands:
                    {
                        this.commandSender.AssignmentOfDampingBands(this.frequencies);
                        break;
                    }

                    case CommandCodes.SetProhibFreqForDampingBands:
                    {
                        this.commandSender.SetProhibitedFreqForDampingBands(this.frequencies);
                        break;
                    }

                    case CommandCodes.SetImpFreqForReconn:
                    {
                        this.commandSender.SetImportantFreqForRadioReconnaissance(this.frequencies);
                        break;
                    }

                    case CommandCodes.SetKnownFreqReconn:
                    {
                        this.commandSender.SetKnownFreqForRadioReconnaissance(this.frequencies);
                        break;
                    }

                    case CommandCodes.AssigningFixedOperFreqForRadioSup:
                    {
                        this.AssigningFixedOperFreqForRadioSup();
                        break;
                    }

                    case CommandCodes.AssigningFreqWithProgramConvForRadioSup:
                    {
                        this.AssigningFreqWithProgramConvForRadioSup();

                        break;
                    }

                    case CommandCodes.RequestForExecutiveDirectionFinding:
                    {
                        this.DirectionFindingRequest(code);
                        break;
                    }

                    case CommandCodes.RequestForQuasiTemporalDirectionFinding:
                    {
                        this.DirectionFindingRequest(code);
                        break;
                    }

                    case CommandCodes.SaveData:
                    {
                        this.commandSender.SaveData();
                        break;
                    }

                    case CommandCodes.RequestStateOfRadioSupOfFixedFreqSources:
                    {
                        this.commandSender.RequestStateOfRadioSupSources(
                            code,
                            byte.Parse(this.LetterValue.Text),
                            int.Parse(this.DelayValue.Text));
                        break;
                    }

                    case CommandCodes.RequestStateOfRadioSupWithProgConvSources:
                    {
                        this.commandSender.RequestStateOfRadioSupSources(
                            code,
                            byte.Parse(this.LetterValue.Text),
                            int.Parse(this.DelayValue.Text));
                        break;
                    }

                    case CommandCodes.StatusRequest:
                    {
                        this.commandSender.DataRequest(code, int.Parse(this.DelayValue.Text));
                        break;
                    }

                    case CommandCodes.CoordRequest:
                    {
                        this.commandSender.DataRequest(code, int.Parse(this.DelayValue.Text));
                        break;
                    }

                    case CommandCodes.RequestForFixFreqRadioSources:
                    {
                        this.commandSender.DataRequest(code, int.Parse(this.DelayValue.Text));
                        break;
                    }

                    case CommandCodes.RequestForProgramConvRadioSources:
                    {
                        this.commandSender.DataRequest(code, int.Parse(this.DelayValue.Text));
                        break;
                    }

                    default:
                    {
                        this.commandSender.TestCommand();
                        break;
                    }
                }
            }
            catch (TaskCanceledException)
            {
                // ignore 
            }
            catch (NullReferenceException)
            {
                this.ConnectButton_OnClick(sender, e);
            }

        }

        private void DirectionFindingRequest(CommandCodes codes)
        {
            var bytesToSend = new[]
                                  {
                                      byte.Parse(this.DirectionRequestFreq1.Text), byte.Parse(this.DirectionRequestFreq2.Text),
                                      byte.Parse(this.DirectionRequestFreq3.Text), byte.Parse(this.DirectionRequestFreq4.Text)
                                  };
            this.commandSender.DirectionFindingRequest(codes, bytesToSend);
        }

        private void AssigningFreqWithProgramConvForRadioSup()
        {
            var radiationDuration = new[]
                                        {
                                            byte.Parse(this.PprchRadDValue1.Text), byte.Parse(this.PprchRadDValue2.Text),
                                            byte.Parse(this.PprchRadDValue3.Text)
                                        };
            var freqsBytes = new[]
                                 {
                                     byte.Parse(this.PprchFreqValue1.Text), byte.Parse(this.PprchFreqValue2.Text),
                                     byte.Parse(this.PprchFreqValue3.Text), byte.Parse(this.PprchFreqValue4.Text)
                                 };
            this.commandSender.AssigningFreqWithProgramConvForRadioSup(
                radiationDuration,
                freqsBytes,
                byte.Parse(this.FFtCodeValue.Text),
                byte.Parse(this.PprchModCodeValue.Text),
                byte.Parse(this.PprchDevCodeValue.Text),
                byte.Parse(this.PprchManCodeValue.Text),
                byte.Parse(this.PprchThresholdValue.Text));
        }

        private void AssigningFixedOperFreqForRadioSup()
        {
            var radiationDuration = new List<byte>
                                        {
                                            byte.Parse(this.RadDValue1.Text),
                                            byte.Parse(this.RadDValue1.Text),
                                            byte.Parse(this.RadDValue3.Text),
                                            byte.Parse(this.RadDValue4.Text)
                                        };
            this.commandSender.AssigningFixedOperFreqForRadioSup(radiationDuration, this.supFreqs);
        }

        /// <summary>
        /// Displays a arguments entry table based on the command selected.
        /// </summary>
        private void ShowArgumentGrid(object o, EventArgs eventArgs)
        {
            if (this.CommandBox.SelectionBoxItem.ToString() == string.Empty)
            {
                return;
            }

            this.CollapseAllArgumentsGrid();

            var code = (CommandCodes)this.CommandBox.SelectionBoxItem;
            switch (code)
            {
                case CommandCodes.SetTimeCommand:
                    {
                        this.TimeArgGrid.Visibility = Visibility.Visible;
                        this.DelayValueGrid.Visibility = Visibility.Visible;
                        this.RemoveClickEvent();
                        this.StopSendButtonClick += (obj, args) =>
                        {
                            this.commandSender.CancelingSendingOfCommands(code);
                        };
                        this.CurrentTime.Text = DateTime.Now.ToString("HH:mm:ss zz");
                        break;
                    }

                case CommandCodes.SetOperMode:
                    {
                        this.SetOperGrid.Visibility = Visibility.Visible;
                        break;
                    }

                case CommandCodes.TextCommand:
                    {
                        this.TextArgGrid.Visibility = Visibility.Visible;
                        break;
                    }

                case CommandCodes.SetRadioReconnRange:
                    {
                        this.SetFreqGrid.Visibility = Visibility.Visible;
                        this.AddFreqValuesButton.Visibility = Visibility.Visible;
                        this.DeleteValuesButton.Visibility = Visibility.Visible;
                        this.frequencies.Clear();
                        break;
                    }
                case CommandCodes.SetRadioReconnSector:
                    {
                        this.SetAngleGrid.Visibility = Visibility.Visible;
                        this.AddAnglesValuesButton.Visibility = Visibility.Visible;
                        this.DeleteValuesButton.Visibility = Visibility.Visible;
                        this.angles.Clear();
                        break;
                    }

                case CommandCodes.AssignmentOfDampingBands:
                    {
                        this.SetFreqGrid.Visibility = Visibility.Visible;
                        this.AddFreqValuesButton.Visibility = Visibility.Visible;
                        this.DeleteValuesButton.Visibility = Visibility.Visible;
                        this.frequencies.Clear();
                        break;
                    }

                case CommandCodes.SetProhibFreqForDampingBands:
                    {
                        this.SetFreqGrid.Visibility = Visibility.Visible;
                        this.AddFreqValuesButton.Visibility = Visibility.Visible;
                        this.DeleteValuesButton.Visibility = Visibility.Visible;
                        this.frequencies.Clear();
                        break;
                    }

                case CommandCodes.SetImpFreqForReconn:
                    {
                        this.SetFreqGrid.Visibility = Visibility.Visible;
                        this.AddFreqValuesButton.Visibility = Visibility.Visible;
                        this.DeleteValuesButton.Visibility = Visibility.Visible;
                        this.frequencies.Clear();
                        break;
                    }

                case CommandCodes.SetKnownFreqReconn:
                    {
                        this.SetFreqGrid.Visibility = Visibility.Visible;
                        this.AddFreqValuesButton.Visibility = Visibility.Visible;
                        this.DeleteValuesButton.Visibility = Visibility.Visible;
                        this.frequencies.Clear();
                        break;
                    }

                case CommandCodes.AssigningFixedOperFreqForRadioSup:
                    {
                        this.SreArgGrid.Visibility = Visibility.Visible;
                        this.AddSupFreqValuesButton.Visibility = Visibility.Visible;
                        this.DeleteValuesButton.Visibility = Visibility.Visible;
                        this.supFreqs.Clear();
                        break;
                    }

                case CommandCodes.AssigningFreqWithProgramConvForRadioSup:
                    {
                        this.PprchArgGrid.Visibility = Visibility.Visible;
                        break;
                    }

                case CommandCodes.RequestForExecutiveDirectionFinding:
                    {
                        this.DirectionRequestGrid.Visibility = Visibility.Visible;
                        break;
                    }

                case CommandCodes.RequestForQuasiTemporalDirectionFinding:
                    {
                        this.DirectionRequestGrid.Visibility = Visibility.Visible;
                        break;
                    }

                case CommandCodes.RequestStateOfRadioSupOfFixedFreqSources:
                    {
                        this.SuppressionRequestGrid.Visibility = Visibility.Visible;
                        this.DelayValueGrid.Visibility = Visibility.Visible;
                        this.RemoveClickEvent();
                        this.StopSendButtonClick += (obj, args) =>
                        {
                            this.commandSender.CancelingSendingOfCommands(code);
                        };
                        break;
                    }

                case CommandCodes.RequestStateOfRadioSupWithProgConvSources:
                    {
                        this.SuppressionRequestGrid.Visibility = Visibility.Visible;
                        this.DelayValueGrid.Visibility = Visibility.Visible;
                        this.RemoveClickEvent();
                        this.StopSendButtonClick += (obj, args) =>
                        {
                            this.commandSender.CancelingSendingOfCommands(code);
                        };
                        break;
                    }

                default:
                    {
                        {
                            this.CollapseAllArgumentsGrid();
                            this.DelayValueGrid.Visibility = Visibility.Visible;
                            this.RemoveClickEvent();
                            this.StopSendButtonClick += (obj, args) =>
                            {
                                this.commandSender.CancelingSendingOfCommands(code);
                            };
                            break;
                        }
                    }
            }
        }

        /// <summary>
        /// The collapse all arguments grids.
        /// </summary>
        private void CollapseAllArgumentsGrid()
        {
            this.TextArgGrid.Visibility = Visibility.Collapsed;
            this.PprchArgGrid.Visibility = Visibility.Collapsed;
            this.SetAngleGrid.Visibility = Visibility.Collapsed;
            this.SetFreqGrid.Visibility = Visibility.Collapsed;
            this.SetOperGrid.Visibility = Visibility.Collapsed;
            this.SreArgGrid.Visibility = Visibility.Collapsed;
            this.TimeArgGrid.Visibility = Visibility.Collapsed;
            this.SuppressionRequestGrid.Visibility = Visibility.Collapsed;
            this.DirectionRequestGrid.Visibility = Visibility.Collapsed;
            this.DelayValueGrid.Visibility = Visibility.Collapsed;
            this.AddFreqValuesButton.Visibility = Visibility.Collapsed;
            this.AddAnglesValuesButton.Visibility = Visibility.Collapsed;
            this.AddSupFreqValuesButton.Visibility = Visibility.Collapsed;
            this.DeleteValuesButton.Visibility = Visibility.Collapsed;
        }

        /// <summary>
        /// Checks the input text against a byte value.
        /// </summary>
        private void TextBox_OnPreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            var textBox = sender as TextBox;
            var fullText = textBox?.Text.Insert(textBox.SelectionStart, e.Text);

            e.Handled = !byte.TryParse(fullText, out _);
        }

        /// <summary>
        /// After entering data in the <see cref="ClientId"/>, changes the value of the client ID
        /// </summary>
        private void IdTextBox_OnTextChanged(object sender, RoutedEventArgs routedEventArgs)
        {
            //if (byte.TryParse(this.ClientId.Text, out _))
            //{
            //    if (this.tcpClient == null)
            //    { 
            //        return;
            //    }

            //    this.tcpClient.ClientId = byte.Parse(this.ClientId.Text);
            //    this.commandSender.SenderAddress = this.tcpClient.ClientId;
            //    this.LogBox.AddTextToLog("Id changed.");
            //}
        }

        private void AddFreqValuesButton_OnClick(object sender, RoutedEventArgs e)
        {
            this.LogBox.AddTextToLog(
                "Values added:" + this.FreqValue1.Text + this.FreqValue2.Text + this.FreqValue3.Text
                + this.FreqValue4.Text + this.FreqValue5.Text + this.FreqValue6.Text + this.FreqValue7.Text
                + this.FreqValue8.Text + "\n",
                Brushes.Ivory);
            this.frequencies.Add(
                new Frequency(
                    new List<byte>
                        {
                            byte.Parse(this.FreqValue1.Text),
                            byte.Parse(this.FreqValue2.Text),
                            byte.Parse(this.FreqValue3.Text),
                            byte.Parse(this.FreqValue4.Text)
                        },
                    new List<byte>
                        {
                            byte.Parse(this.FreqValue5.Text),
                            byte.Parse(this.FreqValue6.Text),
                            byte.Parse(this.FreqValue7.Text),
                            byte.Parse(this.FreqValue8.Text)
                        }));
        }

        private void AddAnglesValuesButton_OnClick(object sender, RoutedEventArgs e)
        {
            this.LogBox.AddTextToLog(
                "Values added:" + this.AngleValue1.Text + this.AngleValue2.Text + this.AngleValue3.Text
                + this.AngleValue4.Text + "\n",
                Brushes.Ivory);
            this.angles.Add(
                new Angle(
                    new List<byte> { byte.Parse(this.AngleValue1.Text), byte.Parse(this.AngleValue2.Text) },
                    new List<byte> { byte.Parse(this.AngleValue3.Text), byte.Parse(this.AngleValue4.Text) }));
        }

        private void AddSupFreqValuesButton_OnClick(object sender, RoutedEventArgs e)
        {
            this.LogBox.AddTextToLog(
                "Values added:" + this.AngleValue1.Text + this.SreFreqValue1.Text + this.SreFreqValue2.Text
                + this.SreFreqValue3.Text + this.SreFreqValue4.Text + this.ModCodeValue.Text + this.DevCodeValue.Text
                + this.ManCodeValue.Text + this.DurationValue.Text + this.PriorityValue.Text + this.ThresholdValue.Text
                + "\n",
                Brushes.Ivory);
            this.supFreqs.Add(
                new SuppressionFrequencies(
                    new List<byte>
                        {
                            byte.Parse(this.SreFreqValue1.Text),
                            byte.Parse(this.SreFreqValue2.Text),
                            byte.Parse(this.SreFreqValue3.Text),
                            byte.Parse(this.SreFreqValue4.Text)
                        },
                    byte.Parse(this.ModCodeValue.Text),
                    byte.Parse(this.DevCodeValue.Text),
                    byte.Parse(this.ManCodeValue.Text),
                    byte.Parse(this.DurationValue.Text),
                    byte.Parse(this.PriorityValue.Text),
                    byte.Parse(this.ThresholdValue.Text)));
        }

        private void RemoveClickEvent()
        {
            foreach (var eventHandler in this.delegateEventHandlers)
            {
                this.StopSend.Click -= eventHandler;
            }

            this.delegateEventHandlers.Clear();
        }

        private void DeleteValuesButton_Click(object sender, RoutedEventArgs e)
        {
            this.supFreqs.Clear();
            this.frequencies.Clear();
            this.angles.Clear();
        }

        private void StopClient_OnClick(object sender, RoutedEventArgs e)
        {
            this.commandSender?.Disconnect();
            this.StopClient.Visibility = Visibility.Collapsed;
            this.ConnectButton.Visibility = Visibility.Visible;
        }
    }
}
