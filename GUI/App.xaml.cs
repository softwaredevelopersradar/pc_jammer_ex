﻿namespace GUI
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Threading;
    using System.Windows;

    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private TcpClientWindow clientWindow;

        public static List<CultureInfo> LanguagesList { get; private set; } = 
            new List<CultureInfo>{ new CultureInfo("en-US"), new CultureInfo("ru-RU") };

        public static event EventHandler LanguageChanged;

        public static CultureInfo Language
        {
            get => Thread.CurrentThread.CurrentCulture;

            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException("value");
                }
                if (value == Thread.CurrentThread.CurrentCulture)
                {
                    return;
                }

                // Create new ResourceDictionary for new culture.
                var dict = new ResourceDictionary();
                switch (value.Name)
                {
                    case "ru-RU":
                        dict.Source = new Uri($"Resources/lang.{value.Name}.xaml", UriKind.Relative);
                        break;
                    default:
                        dict.Source = new Uri("Resources/lang.xaml", UriKind.Relative);
                        break;
                }

                // Switch old ResourceDictionary with new one.
                var oldDict = (from d in Current.Resources.MergedDictionaries
                               where d.Source != null && d.Source.OriginalString.StartsWith("Resources/lang.")
                               select d).First();

                if (oldDict != null)
                {
                    var ind = Current.Resources.MergedDictionaries.IndexOf(oldDict);
                    Current.Resources.MergedDictionaries.Remove(oldDict);
                    Current.Resources.MergedDictionaries.Insert(ind, dict);
                }
                else
                {
                    Current.Resources.MergedDictionaries.Add(dict);
                }

                LanguageChanged?.Invoke(Application.Current, new EventArgs());
            }
        }

        public void AppStartup(object sender, StartupEventArgs eventArgs)
        {
            if ( eventArgs != null && eventArgs.Args.Length > 0)
            {
                if ( LanguagesList.Contains(new CultureInfo(eventArgs.Args[0])))
                {
                    Language = new CultureInfo(eventArgs.Args[0]);
                }
            }

            this.clientWindow = new TcpClientWindow();
            this.clientWindow.Show();
        }

    }
}
