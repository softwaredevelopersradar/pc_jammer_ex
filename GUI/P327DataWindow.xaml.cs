﻿namespace GUI
{
    using System;
    using System.Windows;

    using CommandLibrary.Resources;

    /// <summary>
    /// Interaction logic for P327DataWindow.xaml
    /// </summary>
    public partial class P327DataWindow : Window
    {
        private P327Data data;

        /// <summary>
        /// Initializes a new instance of the <see cref="P327DataWindow"/> class.
        /// </summary>
        /// <param name="p327Data">
        /// The object from which the data would be taken
        /// </param>
        public P327DataWindow(P327Data p327Data)
        {
            this.InitializeComponent();
            this.data = p327Data;
            this.CurrentOperMode.Text = this.data.CurrentOperatingMode.ToString();
            this.ServiceabilityOfLetters.Text = string.Join(",", this.data.ServiceabilityOfLetters);
            this.ChanelControl.Text = this.data.ChanelControl.ToString();
            this.RadioReconnFreqMin.Text = string.Join(",", this.data.RadioReconnFreqMin);
            this.RadioReconnFreqMax.Text = string.Join(",", this.data.RadioReconnFreqMax);
            this.ImpFreqForReconnMin.Text = string.Join(",", this.data.ImpFreqForReconnMin);
            this.ImpFreqForReconnMax.Text = string.Join(",", this.data.ImpFreqForReconnMax);
            this.RadioReconnSectorAngleMin.Text = string.Join(",", this.data.RadioReconnSectorAngleMin);
            this.RadioReconnSectorAngleMax.Text = string.Join(",", this.data.RadioReconnSectorAngleMax);
            this.DampingFreqMin.Text = string.Join(",", this.data.DampingFreqMin);
            this.DampingFreqMax.Text = string.Join(",", this.data.DampingFreqMax);
            this.ProhibDampingFreqMin.Text = string.Join(",", this.data.ProhibDampingFreqMin);
            this.ProhibDampingFreqMax.Text = string.Join(",", this.data.ProhibDampingFreqMax);
            this.RadiationDuration.Text = string.Join(",", this.data.RadiationDuration);
            this.SREFreqValues.Text = string.Join(",", this.data.SreFreqValues);
            this.ModulationCode.Text = string.Join(",", this.data.ModulationCode);
            this.DeviationCode.Text = string.Join(",", this.data.DeviationCode);
            this.ManipulationCode.Text = string.Join(",", this.data.ManipulationCode);
            this.DurationCode.Text = string.Join(",", this.data.DurationCode);
            this.PriorityCode.Text = string.Join(",", this.data.PriorityCode);
            this.ThresholdValue.Text = string.Join(",", this.data.ThresholdValue);
            this.ProgRadiationDuration.Text = string.Join(",", this.data.IriRadiationDuration);
            this.ProgSREFreqValues.Text = string.Join(",", this.data.IriSreFreqValues);
            this.ProgFftResolutionCode.Text = this.data.IriFftResolutionCode.ToString();
            this.ProgModulationCode.Text = this.data.IriModulationCode.ToString();
            this.ProgDeviationCode.Text = this.data.IriDeviationCode.ToString();
            this.ProgManipulationCode.Text = this.data.IriManipulationCode.ToString();
            this.ProgThresholdValue.Text = this.data.IriThresholdValue.ToString();
            this.FixFreqRadioSource.Text = string.Join(",", this.data.DataEmFixedFreq.ToArray());
            this.ProgRadioSource.Text = string.Join(",", this.data.DataEmSoftwareOpFreq.ToArray());
            this.IriJammingData.Text = string.Join(",", this.data.IRiJammingData);
            this.PprchJammingData.Text = string.Join(",", this.data.PprchJammingData.ToArray());
            this.Coordinates.Text = string.Join(",", this.data.Coordinates);
            this.CurrentTime.Text = string.Join(",", this.data.CurrentTime);
            this.XmlFileName.Text = this.data.XmlFilePath;
        }

        /// <summary>
        /// Closes the window without changing data
        /// </summary>
        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        /// <summary>
        /// Stores all entered values ​​into a <see cref="P327Data"/> object.
        /// </summary>
        private void Accept_Click(object sender, RoutedEventArgs e)
        {
            this.data.CurrentOperatingMode = this.ParseArgumentString(this.CurrentOperMode.Text)[0];
            this.data.ServiceabilityOfLetters = this.ParseArgumentString(this.ServiceabilityOfLetters.Text);
            this.data.ChanelControl = this.ParseArgumentString(this.ChanelControl.Text)[0];
            this.data.RadioReconnFreqMin = this.ParseArgumentString(this.RadioReconnFreqMin.Text);
            this.data.RadioReconnFreqMax = this.ParseArgumentString(this.RadioReconnFreqMax.Text);
            this.data.ImpFreqForReconnMin = this.ParseArgumentString(this.ImpFreqForReconnMin.Text);
            this.data.ImpFreqForReconnMax = this.ParseArgumentString(this.ImpFreqForReconnMax.Text);
            this.data.RadioReconnSectorAngleMin = this.ParseArgumentString(this.RadioReconnSectorAngleMin.Text);
            this.data.RadioReconnSectorAngleMax = this.ParseArgumentString(this.RadioReconnSectorAngleMax.Text);
            this.data.DampingFreqMin = this.ParseArgumentString(this.DampingFreqMin.Text);
            this.data.DampingFreqMax = this.ParseArgumentString(this.DampingFreqMax.Text);
            this.data.ProhibDampingFreqMin = this.ParseArgumentString(this.ProhibDampingFreqMin.Text);
            this.data.ProhibDampingFreqMax = this.ParseArgumentString(this.ProhibDampingFreqMax.Text);
            this.data.RadiationDuration = this.ParseArgumentString(this.RadiationDuration.Text);
            this.data.SreFreqValues = this.ParseArgumentString(this.SREFreqValues.Text);
            this.data.ModulationCode = this.ParseArgumentString(this.ModulationCode.Text);
            this.data.DeviationCode = this.ParseArgumentString(this.DeviationCode.Text);
            this.data.ManipulationCode = this.ParseArgumentString(this.ManipulationCode.Text);
            this.data.DurationCode = this.ParseArgumentString(this.DurationCode.Text);
            this.data.PriorityCode = this.ParseArgumentString(this.PriorityCode.Text);
            this.data.ThresholdValue = this.ParseArgumentString(this.ThresholdValue.Text);
            this.data.IriRadiationDuration = this.ParseArgumentString(this.ProgRadiationDuration.Text);
            this.data.IriSreFreqValues = this.ParseArgumentString(this.ProgSREFreqValues.Text);
            this.data.IriFftResolutionCode = this.ParseArgumentString(this.ProgFftResolutionCode.Text)[0];
            this.data.IriModulationCode = this.ParseArgumentString(this.ProgModulationCode.Text)[0];
            this.data.IriDeviationCode = this.ParseArgumentString(this.ProgDeviationCode.Text)[0];
            this.data.IriManipulationCode = this.ParseArgumentString(this.ProgManipulationCode.Text)[0];
            this.data.IriThresholdValue = this.ParseArgumentString(this.ProgThresholdValue.Text)[0];
            this.data.Coordinates = this.ParseArgumentString(this.Coordinates.Text);
            this.data.CurrentTime = this.ParseArgumentString(this.CurrentTime.Text);
            this.data.XmlFilePath = this.XmlFileName.Text;
            this.data.WriteInFile();
            this.DialogResult = true;
        }

        private byte[] ParseArgumentString(string text)
        {
            var splitString = text.Split(',');
            var parsedBytes = new byte[splitString.Length];
            for (var i = 0; i < splitString.Length; i++)
            {
                try
                {
                    parsedBytes[i] = Convert.ToByte(splitString[i]);
                }
                catch (FormatException)
                {
                }
                catch (OverflowException)
                {
                    parsedBytes = new byte[] { 0 };
                }
            }

            return parsedBytes;
        }
    }
}
