﻿
namespace GUI
{
    using System;
    using System.Net;
    using System.Net.Sockets;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using System.Windows.Media;

    using WatsonTcp;

    using CommandLibrary;
    using CommandLibrary.Resources;
    using CommandLibrary.Resources.Enum;


    /// <summary>
    /// Interaction logic for TcpServerWindow.xaml
    /// </summary>
    public partial class TcpServerWindow : Window
    {

        private CommandReceiver receiver;

        /// <summary>
        /// Initializes a new instance of the <see cref="TcpServerWindow"/> class.
        /// </summary>
        public TcpServerWindow()
        {
            this.InitializeComponent();
            this.ListenPortTextBlock.Text = "808";
        }

        /// <summary>
        /// Add event handlers when window is initiated 
        /// </summary>
        private void AddEventHandlers()
        {
            this.receiver.TcpEvents.ClientConnected += this.OnClientConnected;
            this.receiver.TcpEvents.ClientDisconnected += this.OnClientDisconnected;
            this.receiver.TcpEvents.ServerStarted += this.OnServerStarted;
            this.receiver.TcpEvents.ServerStopped += this.OnServerStopped;
            this.receiver.TcpEvents.MessageReceived += this.OnMessageReceived;
        }

        private void OnClientConnected(object o, ConnectionEventArgs args)
        {
            this.Dispatcher.Invoke(() => { this.LogBox.AddTextToLog("Client connected from: " + args.IpPort + "\n", Brushes.Ivory); });
        }

        private void OnClientDisconnected(object o, DisconnectionEventArgs args)
        {
            this.Dispatcher.Invoke(() => { this.LogBox.AddTextToLog("Client disconnected:" + args.IpPort + " reason:" + args.Reason + "\n", Brushes.Ivory); });
        }

        private void OnMessageReceived(object sender, MessageReceivedEventArgs args)
        {
            this.Dispatcher?.Invoke(() =>
            {
                this.LogBox.AddTextToLog("Received from" + args.IpPort + this.ByteOutput(Package.Parse(args.Data), this.LogBox.ShowAllByte), Brushes.Linen);
            });
        }

        private void OnServerStopped(object sender, EventArgs args)
        {
            this.Dispatcher.Invoke(() =>
            {
                this.ConnectionStatusText.Text = "Offline";
                this.LaunchButton.Visibility = Visibility.Visible;
                this.StopServer.Visibility = Visibility.Collapsed;
            });
        }

        private void OnServerStarted(object sender, EventArgs t)
        {
            this.Dispatcher.Invoke(() =>
            {
                this.ConnectionStatusText.Text = "Online";
                this.LaunchButton.Visibility = Visibility.Collapsed;
                this.StopServer.Visibility = Visibility.Visible;
            });
        }

        /// <summary>
        /// Action after window close.
        /// </summary>
        private void TcpServerWindow_OnClosed(object sender, EventArgs e)
        {
            this.receiver?.Dispose();
        }

        /// <summary>
        /// Starts the TCP server, and also forms a command <see cref="receiver"/>.
        /// </summary>
        private void LaunchButton_OnClick(object sender, RoutedEventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(this.ListenPortTextBlock.Text))
                {
                    throw new ArgumentNullException();
                }

                // this.tcpServer.SelectedClientId = byte.Parse(this.ClientId.Text);
                this.receiver = new CommandReceiver(new P327Data(), byte.Parse(this.ServerId.Text), 102,
                    new IPEndPoint(IPAddress.Parse("127.0.0.1"), int.Parse(this.ListenPortTextBlock.Text)));
                this.AddEventHandlers();
                this.receiver.Server.Settings.Logger = this.Logger;
                this.receiver.StartTcpServer();
            }
            catch (ArgumentNullException)
            {
                this.LogBox.AddTextToLog("Please enter server port.\n", Brushes.Red);
            }
            catch (FormatException)
            {
                this.LogBox.AddTextToLog("Wrong server port value.\n", Brushes.Red);
            }
            catch (SocketException)
            {
                this.LogBox.AddTextToLog("Server startup error.\n", Brushes.Red);
            }
        }

        private void Logger(Severity s, string message)
        {
            this.receiver.LoggerFormat = this.LogBox.ShowAllByte ? FormatType.Hex : FormatType.Full;
            this.Dispatcher.Invoke(() =>
            {
                switch (s)
                {
                    case Severity.Debug:
                        this.LogBox.AddTextToLog(message, Brushes.Gray);
                        break;
                    case Severity.Info:
                        this.LogBox.AddTextToLog(message, Brushes.Aquamarine);
                        break;
                    case Severity.Warn:
                        this.LogBox.AddTextToLog(message, Brushes.Yellow);
                        break;
                    case Severity.Error:
                        if (message.Length > 1800)
                        {
                            break; // won't show not informative messages.
                        }
                        this.LogBox.AddTextToLog(message, Brushes.OrangeRed);
                        break;
                    case Severity.Alert:
                        this.LogBox.AddTextToLog(message, Brushes.OrangeRed);
                        break;
                    case Severity.Critical:
                        this.LogBox.AddTextToLog(message, Brushes.Red);
                        break;
                    case Severity.Emergency:
                        this.LogBox.AddTextToLog(message, Brushes.Red);
                        break;
                    default:
                        this.LogBox.AddTextToLog(message, Brushes.Gray);
                        break;
                }
            });
        }

        private string ByteOutput(Package package, bool value)
        {
            return value
                       ? " : " + Enum.GetName(typeof(CommandCodes), package.CommandId) + " " + package.ToString(FormatType.Hex)
                         + "\n\n"
                       : " : " + Enum.GetName(typeof(CommandCodes), package.CommandId) + " " + package.ToString(FormatType.Full)
                         + "\n\n";
        }


        /// <summary>
        /// Checks the input text against a byte value.
        /// </summary>
        private void TextBox_OnPreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            var textBox = sender as TextBox;
            var fullText = textBox?.Text.Insert(textBox.SelectionStart, e.Text);

            e.Handled = !byte.TryParse(fullText, out _);
        }

        private void PortTextBox_OnPreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            var textBox = sender as TextBox;
            var fullText = textBox?.Text.Insert(textBox.SelectionStart, e.Text);

            e.Handled = !ushort.TryParse(fullText, out _);
        }

        private void ShowData_OnClick(object sender, RoutedEventArgs e)
        {
            if(this.receiver == null)
            {
                return;
            }

            var dataWindow = new P327DataWindow(this.receiver.Data);

            if (dataWindow.ShowDialog() == true)
            {
                this.LogBox.AddTextToLog("Data saved.\n", Brushes.Ivory);
            }
        }

        private void StopServer_OnClick(object sender, RoutedEventArgs e)
        {
            this.receiver?.StopServer();
        }
    }
}
